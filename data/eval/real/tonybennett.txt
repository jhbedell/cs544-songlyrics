You're the only reason for my happiness
You're the one I'm always dreaming of
Just to show you what I mean by happiness
Let me paint a picture of my love

As beats the ocean surf upon the sand
So beats my heart for you
For just as constantly as sea meets sand
So beats my heart for you

I'll always be true
Faithful to you
Skies have been blue
Since you came in view

As beats the rhythm of a mighty band
So beats my heart for you, dear
So beats my heart for you

As beats a melody that Ella hums
So beats my heart for you
Just as exciting as the bongo drums
So beats my heart for you

With you standing there
I'm walking on air
Fell debonair
Just like Fred Astaire

As beats the drummer swinging in a band
So beats my heart for you

Just like the beat of the tom toms
The crashes of cymbals and such
Here is my heart and its pounding
Oh baby I love you so much

So beats my heart
So beats my heart
So beats my heart for you
