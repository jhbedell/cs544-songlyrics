title_stop_list = """1 Time
100 Percent Bitch
4 Me
All Alone
B.O.R (Birth Of Rap)
Base For Your Face
Based Hero
Bill Bellamy
California Boy
Cold War
College Bros
Cumin Up
D.O.R. (Death Of Rap)
Do My Job
Dr. Phil
Everything Based
Exhibit 6
Exhibit Based
Fuck KD
Fuckin With Me
Gimme My Swag Back
Got You One
I Got Bitches
I'm God
I'm Miley Cyrus
Justin Bieber
Life's Zombies
Lifes Hard
Like A Martian
Look Like Jesus
Motivation
Pretty Boy
R.I.P. The Rap Game
Rich Bitch
Still Flexin Membership
Stuck Up Bitch
Swag Jerry Rice
We Are The World
We Can Go Down
Whoopie
Wonton Soup"""

url_stop_list = """
"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("lilb.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("lilb1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 117: "Illusions Of G"
# grumpy disconnect

with open("lilb2.lyr", 'w') as fout:
  for i in range(117+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')



