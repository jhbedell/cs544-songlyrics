title_stop_list = """Little Saint Nick
The Man With All The Toys
Santa's Beard
Merry Christmas, Baby
Christmas Day
Frosty The Snowman
We Three Kings Of Orient Are
Blue Christmas
Santa Claus Is Coming To Town
White Christmas
I'll Be Home For Christmas
Auld Lang Syne
All Dressed Up For School
Breakaway
Brian's Back
California Dreaming
Can't Wait Too Long
Celebrate The News
Cindy, Oh Cindy
Dance, Dance, Dance (Alternate Take)
Don't Back Down (Alternate Take)
Ganz Allein (All Alone)
Graduation Day
Guess I'm Dumb
Hang On To Your Ego
Heroes And Villains (Alternate Version)
I Do
It's A Beautiful Day
Judy
Land Ahoy
Let Him Run Wild (Alternate Take)
Long, Tall Texan
Okie From Muskogee
Old Folks At Home / Ol' Man River
Rock And Roll To The Rescue
Seasons In The Sun
Soulful Old Man Sunshine
The Baker Man
The Little Girl I Once Knew
The Little Old Lady From Pasadena
The Lords Prayer
The Monkey's Uncle
Their Hearts Were Full Of Spring
This Song Wants To Sleep With You Tonight
Walk On By
We Got Love
We're Together Again
You're Welcome"""

url_stop_list = """http://www.azlyrics.com/lyrics/beachboys/medleyigetaroundlittledeucecoupe.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("beachboys.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("beachboys1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 108: "How She Boogalooed It"
# grumpy disconnect

with open("beachboys2.lyr", 'w') as fout:
  for i in range(108+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 168: "Make It Big"
# passive aggressive disconnect

with open("beachboys3.lyr", 'w') as fout:
  for i in range(168+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

