title_stop_list = """All I Need
All In Your Name
All Right
Black Widow
Burn Tonight
Butterflies (Remix)
Can You Feel It
Can't Get Outta The Rain
Can't Get Your Weight Off Of Me
Days In Gloucestershire
Elizabeth, I Love You
Fall In Love
Farewell My Summer Love
For All Time
Got The Hots
Hot Street
I Am A Loser
If You Don't Love Me
Let Me Let Go
Mind Is The Magic
Nite Line
On The Line
One More Chance
People Of The World
Ready 2 Win
Seeing Voices
Serious Effect
Shake Your Body (Down To The Ground)
She Got It
She Was Lovin' Me
Shout
Slapstick
Slave To The Rhythm (Feat. Justin Bieber)
Soldier Boy
Someone Put Your Hand Out
Stay
The Girl Is Mine 2008
There Must Be More To Life Than This
This Is It
Todo Para Ti
Trouble
Wanna Be Startin' Somethin' 2008
Water
We're The World (USA For Africa)
What More Can I Give
Work That Body
You Can't Win
You Really Got A Hold On Me"""

url_stop_list = """http://www.azlyrics.com/lyrics/michaeljackson/badafrojackremixdjbuddhaedit.html
http://www.azlyrics.com/lyrics/michaeljackson/loveneverfeltsogood364755.html
http://www.azlyrics.com/lyrics/michaeljackson/speeddemonneroremix.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("jackson.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("jackson1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 90: "Monkey Business"
# grumpy disconnect

with open("jackson2.lyr", 'w') as fout:
  for i in range(90+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 152: "Xscape"
# grumpy disconnect

with open("jackson3.lyr", 'w') as fout:
  for i in range(153+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')


