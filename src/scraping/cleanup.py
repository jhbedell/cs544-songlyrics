artists = ["2pac",
"aerosmith",
"beachboys",
"beegees",
"dianaross",
"jackson",
"jayz",
"lilb",
"redhotchilipeppers",
"rollingstones",
"taylorswift",
"who"]

import os
lyric_files = [f for f in os.listdir() if ".lyr" in f]
lyric_files.sort()

for artist in artists:
  cleanup(artist)

def cleanup(artist):
  output_file = artist + ".lyr"
  with open(output_file, 'w') as fout:
    i = 1
    while i < 5:
      cur_file = artist + str(i) + ".lyr"
      if cur_file in lyric_files:
        with open(cur_file, 'r') as fin:
          for line in fin:
            fout.write(line.replace("<br>", ""))
      else:
        break
      i += 1

def findMetadata(artist):
  metadata = []
  with open(artist+".lyr", 'r') as fin:
    for line in fin:
      if '[' in line:
        metadata.append(line)
  return metadata

def getArtistsMetadata():
  hold = {}
  for artist in artists:
    hold[artist] = findMetadata(artist)
  for (k,v) in hold.items():
    print(k +'\t' + str(len(v)))
  return(hold)

metadict = getArtistsMetadata()


'''
for MJ, after doing out repeats by hand and editing to make remaining metadata consistent:

with open("ftfy.lyr", 'w') as fout:
  with open("jackson.lyr", 'r') as fin:
    for line in fin:
      if '[' not in line:
        fout.write(line)
'''

"""
same with Lil B, keeping the unknown [?]'s:

with open("lilb_old.lyr", 'r') as fin:
  with open("lilb.lyr", 'w') as fout:
    for line in fin:
      if "[" not in line or "[?" in line:
        fout.write(line)

with open("out.lyr", 'w') as fout:
  with open("lilb.lyr", 'r') as fin:
    for line in fin:
      if '<i>' not in line:
        fout.write(line)
      else:
        fout.write(line.replace("<i>", "").replace("</i>", ""))
"""
