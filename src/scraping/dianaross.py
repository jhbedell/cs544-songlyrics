title_stop_list = """Lady Sings The Blues
The Man I Love
Them There Eyes
What A Little Moonlight Can Do
'T Ain't Nobody's Bizness If I Do
Love Is Here To Stay
Fine And Mellow
Lover Man (Oh Where Can You Be)
Gimme A Pigfoot And A Bottle Of Beer
Don't Explain
You've Changed
All Of Me
My Man
Good Morning Heartache
I Cried For You
Strange Fruit
God Bless The Child
The Feeling That We Have
Can I Go On?
He's The Wizard
Soon As I Get Home / Home
You Can't Win
Ease On Down The Road #1
What Would I Do If I Could Feel?
Slide Some Oil To Me
Ease On Down The Road #2
(I'm A) Mean Ole Lion
Ease On Down The Road #3
Be A Lion
Emerald City Sequence
So You Wanted To See The Wizard
Is This What Feeling Gets? (Dorothy's Theme)
Don't Nobody Bring Me No Bad News
A Brand New Day (Everybody Rejoice) / Liberation Ballet
Believe In Yourself (Dorothy)
Believe In Yourself (Reprise)
Home
If We Hold On Together
Love Story"""

url_stop_list = """http://www.azlyrics.com/lyrics/dianaross/untilwemeetagainremixes.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("dianaross.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

# dontrainonmyparade.html and keepaneye.html repeat at 54, 55 and 128, 129
song_titles.remove("Don't Rain On My Parade")
song_titles.remove("Keep An Eye")
song_urls.remove("http://www.azlyrics.com/lyrics/dianaross/dontrainonmyparade.html")
song_urls.remove("http://www.azlyrics.com/lyrics/dianaross/keepaneye.html")

#######################################

with open("dianaross1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 122: "It's Hard For Me To Say"
# grumpy disconnect

with open("dianaross2.lyr", 'w') as fout:
  for i in range(122+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 259: "Voice Of The Heart"
# grumpy disconnect

with open("dianaross3.lyr", 'w') as fout:
  for i in range(259+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

