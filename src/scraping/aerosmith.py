title_stop_list = """Ain't Enough
All Your Love
Angels Eye
Bacon Biscuit Blues
Black Cherry
Blind Man
Can't Stop Messin'
Chip Away The Stone
Come Together
Deuces Are Wild
Devil's Got A New Disguise
Don't Stop
Downtown Charlie
Face
Fall Together
Girls Of Summer
Head First
Helter Skelter
Hollywood
I Ain't Got You
I Don't Want To Miss A Thing
I Love Trash
I'm Not Talking
Lay It Down
Lizard Love
Love Me Like A Bird Dog
Love Me Two Times
Major Barbra
Melt Down
Mother Popcorn
On The Road Again
Once Is Enough
Rattlesnake Shake
Red House
Riff & Roll
Rocket 88
Rockin' Pneumonia And The Boogie Woogie Flu
Scream In Pain
Sedona Sunrise
Sharpshooter
Smokestack Lightning
South Station Blues
Sudona Sunrise
Theme From Spider Man
Three Mile Smile / Reefer Head Woman
Walk On Water
Wayne's World Theme
Wham Bam
What Kind Of Love Are You On?
When I Needed You
Won't Let You Down
Yo Mamma"""

url_stop_list = """
"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("aerosmith.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("aerosmith1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 121: "Shut Up And Dance"
# grumpy disconnect

with open("aerosmith2.lyr", 'w') as fout:
  for i in range(121+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')



