title_stop_list = """100$ Bill
20 Bag Shorty
8 Miles And Runnin
A Million And 1 Questions (Extended)
Analyze This
Big Pimpin' (Extended)
Brooklyn (Go Hard)
Can I Get A... (Video Edit)
Can't Get With That
Can't Knock The Hustle (Remix)
Celebration
Chill
Crew Love
Criminology (Freestyle)
Dead Presidents (Part 1)
Don't You Know
Excuse Me Miss Again
Excuse Me Miss Again (Remix)
Glory
H To The Izzo
Hey Papi
History
In My Lifetime (Original Ski Mix)
In My Lifetime (Remix)
Jockin' Jay-Z (Dope Boy Fresh)
La, La, La (Excuse Me Again)
Marcy To Hollywood
More Money, More Cash, More Hoes (Remix)
Murdergram
People's Court
Stop (Alternate Version)
Stranded (Haiti Mon Amour)
Super Ugly
This Life Forever
Tom Ford (Remix)
We Made History
We Made It
What The Game Made Me
Who You Wit
Wishing On A Star
You're Only A Customer
You're Welcome
Dirt Off Your Shoulder / Lying From You
Big Pimpin' / Papercut
Jigga What / Faint
Numb / Encore
Izzo / In The End
Points Of Authority / 99 Problems / One Step Closer
No Church In The Wild
Lift Off
Niggas In Paris
Otis
Gotta Have It
New Day
That's My Bitch
Welcome To The Jungle
Who Gon Stop Me
Murder To Excellence
Made In America
Why I Love You
Illest Motherfucker Alive
H.A.M
Primetime
The Joy"""

url_stop_list = """http://www.azlyrics.com/lyrics/jayz/anything102969.html
http://www.azlyrics.com/lyrics/jayz/girlsgirlsgirlsremix.html
http://www.azlyrics.com/lyrics/jayz/intro83948.html
http://www.azlyrics.com/lyrics/jayz/intro66414.html
http://www.azlyrics.com/lyrics/jayz/thereturnremix.html
http://www.azlyrics.com/lyrics/jayz/udontknowremix.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("jayz.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("jayz1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 117: "Moment Of Clarity"
# grumpy disconnect

with open("jayz2.lyr", 'w') as fout:
  for i in range(117+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')



