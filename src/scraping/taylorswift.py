title_stop_list = """Last Christmas
Christmases When You Were Mine
Santa Baby
Silent Night
Christmas Must Be Something More
White Christmas
Ain't Nothing 'Bout You
All Night Diner
Am I Ready For Love
American Boy
American Girl
Baby, Don't You Break My Heart Slow
Beautiful Eyes
Being With My Baby
Bette Davis Eyes
Better Off
Breathless
Brought Up That Way
By The Way
Can I Go With You
Closest To A Cowboy
Crazier
Dark Blue Tennessee
Didn't They
Don't Hate Me For Loving You
Drops Of Jupiter
Eyes Open
Fall Back On You
Firefly
Gracie
I Heart ?
I Knew You Were Trouble (Remix)
I Know What I Want
I Wished On A Plane
I'd Lie
Just South Of Knowing Why
Long Time Coming
Lucky You
Made Up You
Mandolin
Monologue Song (La La La)
My Cure
Nashville
Need You Now
Nevermind
One Thing
One Way Ticket
Our Last Night
Permanent Marker
Revenge
Riptide
Ronan
Safe & Sound
Smokey Black Nights
Spinning Around
Stupid Boy
Sweet Tea And God's Graces
Sweeter Than Fiction
Tell Me
Ten Dollars And A Six Pack
The Diary Of Me
There's Your Trouble
Thirteen Blocks (Can't Call It Love)
This Is Really Happening
Thug Story
Today Was A Fairytale
Viva La Vida
Wait For Me
We Were Happy
Welcome Distraction
What Do You Say
What To Wear
Who I've Always Been
Writing Songs About You
Your Anything
Your Face"""

url_stop_list = """http://www.azlyrics.com/lyrics/taylorswift/foreveralwayspianoversion.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("taylorswift.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("taylorswift1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 118: Deseri
# then quit out passive aggressively...

with open("taylorswift2.lyr", 'w') as fout:
  for i in range(___,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

