title_stop_list = """Time After Time
I Fall In Love Too Easily
East Of The Sun (West Of The Moon)
Nancy (With The Laughing Face)
I Thought About You
Night And Day
I've Got The World On A String
I'm Glad There Is You
A Nightingale Sang In Berkeley Square
I Wished On The Moon
You Go To My Head
The Lady Is A Tramp
I See Your Face Before Me
Day In, Day Out
Indian Summer
Call Me Irresponsible
Here's That Rainy Day
Last Night When We Were Young
I Wish I Were In Love Again
A Foggy Day
Don't Worry 'bout Me
One For My Baby
Angel Eyes
I'll Be Seeing You
I'll Be Home For Christmas
Silver Bells
All I Want For Christmas Is You
My Favorite Things
Christmas Time Is Here
Winter Wonderland
Have Yourself A Merry Little Christmas
Santa Claus Is Coming To Town
I've Got My Love To Keep Me Warm
The Christmas Waltz
O Christmas Tree
Anything Goes
Cheek To Cheek
Don't Wait Too Long
I Can't Give You Anything But Love
Nature Boy
Goody Goody
Ev'ry Time We Say Goodbye
Firefly
I Won't Dance
They All Laughed
Lush Life
Sophisticated Lady
Let's Face The Music And Dance
But Beautiful
It Don't Mean A Thing (If It Ain't Got That Swing)
Bang Bang (My Baby Shot Me Down)
On A Clear Day (You Can See Forever)
Bewitched, Bothered And Bewildered
The Lady's In Love With You
The Lady Is A Tramp"""

url_stop_list = """http://www.azlyrics.com/lyrics/tonybennett/butbeautiful376969.html
http://www.azlyrics.com/lyrics/tonybennett/bymyself227981.html
http://www.azlyrics.com/lyrics/tonybennett/dancinginthedark227968.html
http://www.azlyrics.com/lyrics/tonybennett/dontgetaroundmuchanymore227527.html
http://www.azlyrics.com/lyrics/tonybennett/firefly228082.html
http://www.azlyrics.com/lyrics/tonybennett/firefly376963.html
http://www.azlyrics.com/lyrics/tonybennett/foronceinmylife227540.html
http://www.azlyrics.com/lyrics/tonybennett/gonewiththewind228261.html
http://www.azlyrics.com/lyrics/tonybennett/howlonghasthisbeengoingon227461.html
http://www.azlyrics.com/lyrics/tonybennett/ileftmyheartinsanfrancisco227436.html
http://www.azlyrics.com/lyrics/tonybennett/imalwayschasingrainbows227446.html
http://www.azlyrics.com/lyrics/tonybennett/ifiruledtheworld227492.html
http://www.azlyrics.com/lyrics/tonybennett/ithadtobeyou227458.html
http://www.azlyrics.com/lyrics/tonybennett/justintime227283.html
http://www.azlyrics.com/lyrics/tonybennett/keepsmilingattrouble227545.html
http://www.azlyrics.com/lyrics/tonybennett/keepsmilingattrouble227763.html
http://www.azlyrics.com/lyrics/tonybennett/letsfacethemusicanddance376968.html
http://www.azlyrics.com/lyrics/tonybennett/lullabyofbroadway227253.html
http://www.azlyrics.com/lyrics/tonybennett/lullabyofbroadway227293.html
http://www.azlyrics.com/lyrics/tonybennett/onthesunnysideofthestreet227525.html
http://www.azlyrics.com/lyrics/tonybennett/onthesunnysideofthestreet227807.html
http://www.azlyrics.com/lyrics/tonybennett/outofthisworld227542.html
http://www.azlyrics.com/lyrics/tonybennett/outofthisworld227760.html
http://www.azlyrics.com/lyrics/tonybennett/poorbutterfly228040.html
http://www.azlyrics.com/lyrics/tonybennett/putonahappyface228077.html
http://www.azlyrics.com/lyrics/tonybennett/rulesoftheroad227460.html
http://www.azlyrics.com/lyrics/tonybennett/shesfunnythatway228058.html
http://www.azlyrics.com/lyrics/tonybennett/smile227439.html
http://www.azlyrics.com/lyrics/tonybennett/smile227509.html
http://www.azlyrics.com/lyrics/tonybennett/speaklow227310.html
http://www.azlyrics.com/lyrics/tonybennett/speaklow227457.html
http://www.azlyrics.com/lyrics/tonybennett/thebestisyettocome227447.html
http://www.azlyrics.com/lyrics/tonybennett/theshadowofyoursmile227508.html
http://www.azlyrics.com/lyrics/tonybennett/theverythoughtofyou227515.html
http://www.azlyrics.com/lyrics/tonybennett/thewayyoulooktonight227274.html
http://www.azlyrics.com/lyrics/tonybennett/theyalllaughed376965.html
http://www.azlyrics.com/lyrics/tonybennett/theycanttakethatawayfromme227536.html
http://www.azlyrics.com/lyrics/tonybennett/theycanttakethatawayfromme227758.html
http://www.azlyrics.com/lyrics/tonybennett/theycanttakethatawayfromme227967.html
http://www.azlyrics.com/lyrics/tonybennett/thisfunnyworld227875.html
http://www.azlyrics.com/lyrics/tonybennett/thisisalliask227318.html
http://www.azlyrics.com/lyrics/tonybennett/thisisalliask227425.html
http://www.azlyrics.com/lyrics/tonybennett/whenyouwishuponastar228083.html
http://www.azlyrics.com/lyrics/tonybennett/youtookadvantageofme227844.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
songlist = []
source = ""
with open("tonybennett.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("tonybennett1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Have to stop and restart it with new IP address after ~80-90 requests.
# First time running it, got to 90: "Home is the Place"

with open("tonybennett2.lyr", 'w') as fout:
  for i in range(90,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 173: "Marry Young"

with open("tonybennett3.lyr", 'w') as fout:
  for i in range(173,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 263: "The Beat Of My Heart"

with open("tonybennett4.lyr", 'w') as fout:
  for i in range(263,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Complete!

'''
["'long About Now",
 '(Where Do I Begin) Love Story',
 'A Beautiful Friendship',
 'A Cottage For Sale',
 'A Lonely Place',
 'A Stranger In Paradise',
 'A Taste Of Honey',
 'A Time For Love',
 "Ain't Misbehavin'",
 'Alfie',
 'All For You',
 "All God's Chillun Got Rhythm",
 'All In Fun',
 'All My Tomorrows',
 'All Of Me',
 'All Of You',
 'Alone Together',
 'Antonia',
 'April In Paris',
 "Are You Havin' Any Fun?",
 'As Time Goes By',
 'At Long Last Love',
 'Autumn In Rome',
 'Autumn Leaves',
 "Baby Don't You Quit Now",
 'Baby, Dream Your Dream',
 "Be Careful, It's My Heart",
 'Because Of You',
 'Begin The Beguine',
 'Being Alive',
 'Best Man',
 'Best Thing Is To Be A Person',
 'Between The Devil And The Deep Blue Sea',
 'Blue Moon',
 'Blue Velvet',
 'Blues In The Night',
 'Body And Soul',
 'Boulevard Of Broken Dreams',
 'Bridges (Travessia)',
 'Brightest Smile In Town',
 'Broadway Medley',
 'By Myself',
 'Can You Find It In Your Heart',
 "Can't Get Out Of This Mood",
 'Candy Kisses',
 'Christmasland',
 'Close Enough For Love',
 'Close Your Eyes',
 'Cloudy Morning',
 'Coco',
 'Coffee Break',
 'Cold, Cold Heart',
 'Cole Porter Medley',
 'Come Saturday Morning',
 'Country Girl',
 'Crazy Rhythm',
 'Crazy She Calls Me',
 'Dancing In The Dark',
 'Dat Dere',
 'Daybreak',
 'Days Of Love',
 'Days Of Wine And Roses',
 "Don't Get Around Much Anymore",
 "Don't Like Goodbyes",
 'Down In The Depths On The Ninetieth Floor',
 'Dream',
 'Easy Come, Easy Go',
 'Emily',
 'Every Time We Say Goodbye',
 "Everybody's Talkin'",
 'Experiment',
 'Falling In Love With Love',
 "Fascinatin' Rhythm",
 'Fly Me To The Moon (In Other Words)',
 'Fool Of Fools',
 'Fools Rush In',
 'For Once In My Life',
 'Georgia Rose',
 'Get Happy',
 'Girl Talk',
 'God Bless The Child',
 'Gone With The Wind',
 'Good Morning Heartache',
 'Got Her Off My Hands',
 'Got The Gate On The Golden Gate',
 'Happiness Is A Thing Called Joe',
 'Harlem Butterfly',
 'Have I Told You Lately?',
 'Have You Met Miss Jones?',
 'Hi-Ho',
 'Home Is The Place',
 'Honeysuckle Rose',
 'How Beautiful Is Night',
 'How Do You Keep The Music Playing?',
 'How Do You Say Auf Wiedersehen?',
 'How Insensitive',
 'How Little We Know',
 'How Long Has This Been Going On',
 'Hushabye Mountain',
 'I Concentrate On You',
 'I Could Write A Book',
 'I Cover The Waterfront',
 "I Didn't Know What Time It Was",
 'I Do Not Know A Day I Did Not Love You',
 "I Don't Know Why",
 'I Got Rhythm',
 "I Guess I'll Have To Change My Plan",
 'I Left My Heart In San Francisco',
 'I Let A Song Go Out Of My Heart',
 'I Only Have Eyes For You',
 'I Remember You',
 'I Used To Be Color Blind',
 'I Walk A Little Faster',
 'I Wanna Be Around',
 'I Want To Be Happy',
 "I'll Begin Again",
 "I'll Only Miss Her When I Think Of Her",
 "I'm A Fool To Want You",
 "I'm Always Chasing Rainbows",
 "I'm Losing My Mind",
 "I'm Thru With Love",
 "I've Got Five Dollars",
 "I've Got Just About Everything",
 "I've Gotta Be Me",
 "I've Never Seen",
 "I, Yes Me, That's Who!",
 'If I Could Be With You (One Hour Tonight)',
 'If I Could Go Back',
 'If I Ruled The World',
 'Ill Wind',
 'In The Middle Of An Island',
 'In The Wee Small Hours',
 'Irena',
 "Isn't It Romantic?",
 'It Amazes Me',
 'It Could Happen To You',
 'It Had To Be You',
 'It Only Happens When I Dance With You',
 'It Was Me',
 "It's A Sin To Tell A Lie",
 "It's Magic",
 "It's Only A Paper Moon",
 'Judy',
 'Just Friends',
 'Just In Time',
 'Just One Of Those Things',
 'Keep Smiling At Trouble',
 'Lady Is A Tramp',
 'Laughing At Life',
 'Laura',
 'Lazy Afternoon',
 'Lazy Day',
 'Let There Be Love',
 "Let's Begin",
 'Life Is Beautiful',
 'Listen, Little Girl',
 'Little Did I Dream',
 'Little Green Apples',
 'Little Things',
 'Long Ago And Far Away',
 'Lost In The Stars',
 'Love',
 'Love For Sale',
 'Love Is Here To Stay',
 'Love Is The Thing',
 'Love Scene',
 'Lover',
 'Lullaby Of Broadway',
 'Make It Easy On Yourself',
 'Make Someone Happy',
 "Mam'selle",
 'Man That Got Away',
 'Manhattan',
 'Marry Young',
 'Maybe September',
 'Maybe This Time',
 'Me, Myself And I',
 'Minha (All Mine)',
 'Moonlight In Vermont',
 'More And More',
 'Most Beautiful Girl In The World',
 'Mountain Greenery',
 'My Foolish Heart',
 'My Funny Valentine',
 'My Heart Stood Still',
 'My Ideal',
 'My Inamorata',
 'My Love Went To London',
 'My Mom',
 'My Old Flame',
 'My Romance',
 'Never Too Late',
 'Nice Work If You Can Get It',
 'Nobody Else But Me',
 "Nobody's Heart",
 'Oh, You Crazy Moon',
 "Ol' Man River",
 'Old Devil Moon',
 'On Green Dolphin Street',
 'On The Other Side Of The Tracks',
 'On The Sunny Side Of The Street',
 'Once In A Garden',
 'Once Upon A Time',
 'Only The Young',
 'Out Of This World',
 'Over The Sun',
 'Pawnbroker',
 'Pennies From Heaven',
 "Penthouse Serenade (When We're Alone)",
 'People',
 'Play It Again Sam',
 'Poor Butterfly',
 'Put On A Happy Face',
 'Rags To Riches',
 "Rain, Rain (Don't Go Away)",
 'Reflections',
 'Remind Me',
 'Rules Of The Road',
 'Samba De Orfeu',
 'Samba Do Avião',
 "Sandy's Smile",
 'Sentimental Journey',
 'September Song',
 'Shall We Dance',
 "She's Funny That Way",
 'Shine On Your Shoes',
 'Sing You Sinners',
 "Sleepin' Bee",
 'Sleepy Time Gal',
 'Smile',
 'Snowfall',
 'So Beats My Heart For You',
 'So Far',
 'So Long, Big Time!',
 'Solitude',
 'Some Other Spring',
 'Some Other Time',
 'Something',
 'Something In Your Smile',
 "Sometimes I'm Happy",
 'Somewhere Along The Line',
 'Somewhere Over The Rainbow',
 'Speak Low',
 'Spring Is Here',
 'Stella By Starlight',
 "Steppin' Out With My Baby",
 'Stranger In Paradise',
 'Street Of Dreams',
 'Suddenly',
 'Sweet Georgie Fame',
 'Sweet Lorraine',
 'Swinging On A Star',
 'Take The Moment',
 'Taking A Chance On Love',
 'Tangerine',
 'Tea For Two',
 "Tell Her It's Snowing",
 'Tender Is The Night',
 'Tenderly',
 'That Night',
 'That Ole Devil Called Love',
 "That's Entertainment!",
 'The Bare Necessities',
 'The Beat Of My Heart',
 'The Best Is Yet To Come',
 'The Boulevard Of Broken Dreams',
 'The Gentle Rain',
 'The Girl I Love',
 'The Good Life',
 'The Hands Of Time',
 'The Inch Worm',
 'The Long And Winding Road',
 'The Moment Of Truth',
 'The Playground',
 'The Right To Love',
 'The Riviera',
 'The Second Time Around',
 'The Shadow Of Your Smile',
 'The Shining Sea',
 'The Skyscraper Blues',
 'The Summer Knows',
 'The Trolley Song',
 'The Very Thought Of You',
 'The Way That I Feel',
 'The Way You Look Tonight',
 'Theme From Valley Of The Dolls',
 'Then Was Then And Now Is Now',
 'There Will Never Be Another You',
 "There'll Be Some Changes Made",
 "There's A Lull In My Life",
 "There's A Small Hotel",
 'These Foolish Things (Remind Me Of You)',
 "They Can't Take That Away From Me",
 "Think How It's Gonna Be",
 "This Can't Be Love",
 'This Funny World',
 'This Is All I Ask',
 'Thou Swell',
 'Time To Smile',
 'Toot, Toot, Tootsie! (Goodbye)',
 'Top Hat, White Tie And Tails',
 'Touch Of Your Lips',
 'Touch The Earth',
 'Trapped In A Web Of Love',
 "Trav'lin' Light",
 'Tricks',
 'True Blue Lou',
 'Twilight World',
 'Two By Two',
 'Wait Till You See Her',
 'Walkabout',
 'Waltz For Debby',
 'Watch What Happens',
 'Wave',
 "We Mustn't Say Goodbye",
 "We'll Be Together Again",
 'What A Little Moonlight Can Do',
 'What A Wonderful World',
 'What Makes It Happen',
 'What The World Needs Now Is Love',
 'When A Woman Loves A Man',
 'When Do The Bells Ring For Me',
 'When I Fall In Love',
 'When I Look In Your Eyes',
 'When In Rome',
 'When Lights Are Low',
 'When You Wish Upon A Star',
 'Where Do You Start',
 'Where Or When',
 'White Christmas',
 'Who Can I Turn To',
 'Who Can I Turn To (When Nobody Needs Me)',
 'Who Cares?',
 'Whoever You Are I Love You',
 'Willow Weep For Me',
 'Without A Song',
 'Wonderful One',
 'Wrap Your Troubles In Dreams (And Dream Your Troubles Away)',
 'Yellow Days',
 'Yesterday I Heard The Rain',
 'Yesterdays',
 "You Don't Know What Love Is",
 'You Showed Me The Way',
 'You Took Advantage Of Me',
 "You're All The World To Me",
 "You're Easy To Dance With / Change Partners / Cheek To Cheek",
 'Young And Foolish']
'''

'''
['http://www.azlyrics.com/lyrics/tonybennett/longaboutnow.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wheredoibeginlovestory.html',
 'http://www.azlyrics.com/lyrics/tonybennett/abeautifulfriendship.html',
 'http://www.azlyrics.com/lyrics/tonybennett/acottageforsale.html',
 'http://www.azlyrics.com/lyrics/tonybennett/alonelyplace.html',
 'http://www.azlyrics.com/lyrics/tonybennett/astrangerinparadise.html',
 'http://www.azlyrics.com/lyrics/tonybennett/atasteofhoney.html',
 'http://www.azlyrics.com/lyrics/tonybennett/atimeforlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/aintmisbehavin.html',
 'http://www.azlyrics.com/lyrics/tonybennett/alfie.html',
 'http://www.azlyrics.com/lyrics/tonybennett/allforyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/allgodschillungotrhythm.html',
 'http://www.azlyrics.com/lyrics/tonybennett/allinfun.html',
 'http://www.azlyrics.com/lyrics/tonybennett/allmytomorrows.html',
 'http://www.azlyrics.com/lyrics/tonybennett/allofme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/allofyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/alonetogether.html',
 'http://www.azlyrics.com/lyrics/tonybennett/antonia.html',
 'http://www.azlyrics.com/lyrics/tonybennett/aprilinparis.html',
 'http://www.azlyrics.com/lyrics/tonybennett/areyouhavinanyfun.html',
 'http://www.azlyrics.com/lyrics/tonybennett/astimegoesby.html',
 'http://www.azlyrics.com/lyrics/tonybennett/atlonglastlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/autumninrome.html',
 'http://www.azlyrics.com/lyrics/tonybennett/autumnleaves.html',
 'http://www.azlyrics.com/lyrics/tonybennett/babydontyouquitnow.html',
 'http://www.azlyrics.com/lyrics/tonybennett/babydreamyourdream.html',
 'http://www.azlyrics.com/lyrics/tonybennett/becarefulitsmyheart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/becauseofyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/beginthebeguine.html',
 'http://www.azlyrics.com/lyrics/tonybennett/beingalive.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bestman.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bestthingistobeaperson.html',
 'http://www.azlyrics.com/lyrics/tonybennett/betweenthedevilandthedeepbluesea.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bluemoon.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bluevelvet.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bluesinthenight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bodyandsoul.html',
 'http://www.azlyrics.com/lyrics/tonybennett/boulevardofbrokendreams.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bridgestravessia.html',
 'http://www.azlyrics.com/lyrics/tonybennett/brightestsmileintown.html',
 'http://www.azlyrics.com/lyrics/tonybennett/broadwaymedley.html',
 'http://www.azlyrics.com/lyrics/tonybennett/bymyself.html',
 'http://www.azlyrics.com/lyrics/tonybennett/canyoufinditinyourheart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/cantgetoutofthismood.html',
 'http://www.azlyrics.com/lyrics/tonybennett/candykisses.html',
 'http://www.azlyrics.com/lyrics/tonybennett/christmasland.html',
 'http://www.azlyrics.com/lyrics/tonybennett/closeenoughforlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/closeyoureyes.html',
 'http://www.azlyrics.com/lyrics/tonybennett/cloudymorning.html',
 'http://www.azlyrics.com/lyrics/tonybennett/coco.html',
 'http://www.azlyrics.com/lyrics/tonybennett/coffeebreak.html',
 'http://www.azlyrics.com/lyrics/tonybennett/coldcoldheart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/coleportermedley.html',
 'http://www.azlyrics.com/lyrics/tonybennett/comesaturdaymorning.html',
 'http://www.azlyrics.com/lyrics/tonybennett/countrygirl.html',
 'http://www.azlyrics.com/lyrics/tonybennett/crazyrhythm.html',
 'http://www.azlyrics.com/lyrics/tonybennett/crazyshecallsme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/dancinginthedark.html',
 'http://www.azlyrics.com/lyrics/tonybennett/datdere.html',
 'http://www.azlyrics.com/lyrics/tonybennett/daybreak.html',
 'http://www.azlyrics.com/lyrics/tonybennett/daysoflove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/daysofwineandroses.html',
 'http://www.azlyrics.com/lyrics/tonybennett/dontgetaroundmuchanymore.html',
 'http://www.azlyrics.com/lyrics/tonybennett/dontlikegoodbyes.html',
 'http://www.azlyrics.com/lyrics/tonybennett/downinthedepthsontheninetiethfloor.html',
 'http://www.azlyrics.com/lyrics/tonybennett/dream.html',
 'http://www.azlyrics.com/lyrics/tonybennett/easycomeeasygo.html',
 'http://www.azlyrics.com/lyrics/tonybennett/emily.html',
 'http://www.azlyrics.com/lyrics/tonybennett/everytimewesaygoodbye.html',
 'http://www.azlyrics.com/lyrics/tonybennett/everybodystalkin.html',
 'http://www.azlyrics.com/lyrics/tonybennett/experiment.html',
 'http://www.azlyrics.com/lyrics/tonybennett/fallinginlovewithlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/fascinatinrhythm.html',
 'http://www.azlyrics.com/lyrics/tonybennett/flymetothemooninotherwords.html',
 'http://www.azlyrics.com/lyrics/tonybennett/fooloffools.html',
 'http://www.azlyrics.com/lyrics/tonybennett/foolsrushin.html',
 'http://www.azlyrics.com/lyrics/tonybennett/foronceinmylife.html',
 'http://www.azlyrics.com/lyrics/tonybennett/georgiarose.html',
 'http://www.azlyrics.com/lyrics/tonybennett/gethappy.html',
 'http://www.azlyrics.com/lyrics/tonybennett/girltalk.html',
 'http://www.azlyrics.com/lyrics/tonybennett/godblessthechild.html',
 'http://www.azlyrics.com/lyrics/tonybennett/gonewiththewind.html',
 'http://www.azlyrics.com/lyrics/tonybennett/goodmorningheartache.html',
 'http://www.azlyrics.com/lyrics/tonybennett/gotheroffmyhands.html',
 'http://www.azlyrics.com/lyrics/tonybennett/gotthegateonthegoldengate.html',
 'http://www.azlyrics.com/lyrics/tonybennett/happinessisathingcalledjoe.html',
 'http://www.azlyrics.com/lyrics/tonybennett/harlembutterfly.html',
 'http://www.azlyrics.com/lyrics/tonybennett/haveitoldyoulately.html',
 'http://www.azlyrics.com/lyrics/tonybennett/haveyoumetmissjones.html',
 'http://www.azlyrics.com/lyrics/tonybennett/hiho.html',
 'http://www.azlyrics.com/lyrics/tonybennett/homeistheplace.html',
 'http://www.azlyrics.com/lyrics/tonybennett/honeysucklerose.html',
 'http://www.azlyrics.com/lyrics/tonybennett/howbeautifulisnight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/howdoyoukeepthemusicplaying.html',
 'http://www.azlyrics.com/lyrics/tonybennett/howdoyousayaufwiedersehen.html',
 'http://www.azlyrics.com/lyrics/tonybennett/howinsensitive.html',
 'http://www.azlyrics.com/lyrics/tonybennett/howlittleweknow.html',
 'http://www.azlyrics.com/lyrics/tonybennett/howlonghasthisbeengoingon.html',
 'http://www.azlyrics.com/lyrics/tonybennett/hushabyemountain.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iconcentrateonyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/icouldwriteabook.html',
 'http://www.azlyrics.com/lyrics/tonybennett/icoverthewaterfront.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ididntknowwhattimeitwas.html',
 'http://www.azlyrics.com/lyrics/tonybennett/idonotknowadayididnotloveyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/idontknowwhy.html',
 'http://www.azlyrics.com/lyrics/tonybennett/igotrhythm.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iguessillhavetochangemyplan.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ileftmyheartinsanfrancisco.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iletasonggooutofmyheart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ionlyhaveeyesforyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/irememberyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iusedtobecolorblind.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iwalkalittlefaster.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iwannabearound.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iwanttobehappy.html',
 'http://www.azlyrics.com/lyrics/tonybennett/illbeginagain.html',
 'http://www.azlyrics.com/lyrics/tonybennett/illonlymissherwhenithinkofher.html',
 'http://www.azlyrics.com/lyrics/tonybennett/imafooltowantyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/imalwayschasingrainbows.html',
 'http://www.azlyrics.com/lyrics/tonybennett/imlosingmymind.html',
 'http://www.azlyrics.com/lyrics/tonybennett/imthruwithlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ivegotfivedollars.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ivegotjustabouteverything.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ivegottabeme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iveneverseen.html',
 'http://www.azlyrics.com/lyrics/tonybennett/iyesmethatswho.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ificouldbewithyouonehourtonight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ificouldgoback.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ifiruledtheworld.html',
 'http://www.azlyrics.com/lyrics/tonybennett/illwind.html',
 'http://www.azlyrics.com/lyrics/tonybennett/inthemiddleofanisland.html',
 'http://www.azlyrics.com/lyrics/tonybennett/intheweesmallhours.html',
 'http://www.azlyrics.com/lyrics/tonybennett/irena.html',
 'http://www.azlyrics.com/lyrics/tonybennett/isntitromantic.html',
 'http://www.azlyrics.com/lyrics/tonybennett/itamazesme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/itcouldhappentoyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ithadtobeyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/itonlyhappenswhenidancewithyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/itwasme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/itsasintotellalie.html',
 'http://www.azlyrics.com/lyrics/tonybennett/itsmagic.html',
 'http://www.azlyrics.com/lyrics/tonybennett/itsonlyapapermoon.html',
 'http://www.azlyrics.com/lyrics/tonybennett/judy.html',
 'http://www.azlyrics.com/lyrics/tonybennett/justfriends.html',
 'http://www.azlyrics.com/lyrics/tonybennett/justintime.html',
 'http://www.azlyrics.com/lyrics/tonybennett/justoneofthosethings.html',
 'http://www.azlyrics.com/lyrics/tonybennett/keepsmilingattrouble.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ladyisatramp.html',
 'http://www.azlyrics.com/lyrics/tonybennett/laughingatlife.html',
 'http://www.azlyrics.com/lyrics/tonybennett/laura.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lazyafternoon.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lazyday.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lettherebelove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/letsbegin.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lifeisbeautiful.html',
 'http://www.azlyrics.com/lyrics/tonybennett/listenlittlegirl.html',
 'http://www.azlyrics.com/lyrics/tonybennett/littledididream.html',
 'http://www.azlyrics.com/lyrics/tonybennett/littlegreenapples.html',
 'http://www.azlyrics.com/lyrics/tonybennett/littlethings.html',
 'http://www.azlyrics.com/lyrics/tonybennett/longagoandfaraway.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lostinthestars.html',
 'http://www.azlyrics.com/lyrics/tonybennett/love.html',
 'http://www.azlyrics.com/lyrics/tonybennett/loveforsale.html',
 'http://www.azlyrics.com/lyrics/tonybennett/loveisheretostay.html',
 'http://www.azlyrics.com/lyrics/tonybennett/loveisthething.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lovescene.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lover.html',
 'http://www.azlyrics.com/lyrics/tonybennett/lullabyofbroadway.html',
 'http://www.azlyrics.com/lyrics/tonybennett/makeiteasyonyourself.html',
 'http://www.azlyrics.com/lyrics/tonybennett/makesomeonehappy.html',
 'http://www.azlyrics.com/lyrics/tonybennett/mamselle.html',
 'http://www.azlyrics.com/lyrics/tonybennett/manthatgotaway.html',
 'http://www.azlyrics.com/lyrics/tonybennett/manhattan.html',
 'http://www.azlyrics.com/lyrics/tonybennett/marryyoung.html',
 'http://www.azlyrics.com/lyrics/tonybennett/maybeseptember.html',
 'http://www.azlyrics.com/lyrics/tonybennett/maybethistime.html',
 'http://www.azlyrics.com/lyrics/tonybennett/memyselfandi.html',
 'http://www.azlyrics.com/lyrics/tonybennett/minhaallmine.html',
 'http://www.azlyrics.com/lyrics/tonybennett/moonlightinvermont.html',
 'http://www.azlyrics.com/lyrics/tonybennett/moreandmore.html',
 'http://www.azlyrics.com/lyrics/tonybennett/mostbeautifulgirlintheworld.html',
 'http://www.azlyrics.com/lyrics/tonybennett/mountaingreenery.html',
 'http://www.azlyrics.com/lyrics/tonybennett/myfoolishheart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/myfunnyvalentine.html',
 'http://www.azlyrics.com/lyrics/tonybennett/myheartstoodstill.html',
 'http://www.azlyrics.com/lyrics/tonybennett/myideal.html',
 'http://www.azlyrics.com/lyrics/tonybennett/myinamorata.html',
 'http://www.azlyrics.com/lyrics/tonybennett/mylovewenttolondon.html',
 'http://www.azlyrics.com/lyrics/tonybennett/mymom.html',
 'http://www.azlyrics.com/lyrics/tonybennett/myoldflame.html',
 'http://www.azlyrics.com/lyrics/tonybennett/myromance.html',
 'http://www.azlyrics.com/lyrics/tonybennett/nevertoolate.html',
 'http://www.azlyrics.com/lyrics/tonybennett/niceworkifyoucangetit.html',
 'http://www.azlyrics.com/lyrics/tonybennett/nobodyelsebutme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/nobodysheart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ohyoucrazymoon.html',
 'http://www.azlyrics.com/lyrics/tonybennett/olmanriver.html',
 'http://www.azlyrics.com/lyrics/tonybennett/olddevilmoon.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ongreendolphinstreet.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ontheothersideofthetracks.html',
 'http://www.azlyrics.com/lyrics/tonybennett/onthesunnysideofthestreet.html',
 'http://www.azlyrics.com/lyrics/tonybennett/onceinagarden.html',
 'http://www.azlyrics.com/lyrics/tonybennett/onceuponatime.html',
 'http://www.azlyrics.com/lyrics/tonybennett/onlytheyoung.html',
 'http://www.azlyrics.com/lyrics/tonybennett/outofthisworld.html',
 'http://www.azlyrics.com/lyrics/tonybennett/overthesun.html',
 'http://www.azlyrics.com/lyrics/tonybennett/pawnbroker.html',
 'http://www.azlyrics.com/lyrics/tonybennett/penniesfromheaven.html',
 'http://www.azlyrics.com/lyrics/tonybennett/penthouseserenadewhenwerealone.html',
 'http://www.azlyrics.com/lyrics/tonybennett/people.html',
 'http://www.azlyrics.com/lyrics/tonybennett/playitagainsam.html',
 'http://www.azlyrics.com/lyrics/tonybennett/poorbutterfly.html',
 'http://www.azlyrics.com/lyrics/tonybennett/putonahappyface.html',
 'http://www.azlyrics.com/lyrics/tonybennett/ragstoriches.html',
 'http://www.azlyrics.com/lyrics/tonybennett/rainraindontgoaway.html',
 'http://www.azlyrics.com/lyrics/tonybennett/reflections.html',
 'http://www.azlyrics.com/lyrics/tonybennett/remindme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/rulesoftheroad.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sambadeorfeu.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sambadoavio.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sandyssmile.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sentimentaljourney.html',
 'http://www.azlyrics.com/lyrics/tonybennett/septembersong.html',
 'http://www.azlyrics.com/lyrics/tonybennett/shallwedance.html',
 'http://www.azlyrics.com/lyrics/tonybennett/shesfunnythatway.html',
 'http://www.azlyrics.com/lyrics/tonybennett/shineonyourshoes.html',
 'http://www.azlyrics.com/lyrics/tonybennett/singyousinners.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sleepinbee.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sleepytimegal.html',
 'http://www.azlyrics.com/lyrics/tonybennett/smile.html',
 'http://www.azlyrics.com/lyrics/tonybennett/snowfall.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sobeatsmyheartforyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sofar.html',
 'http://www.azlyrics.com/lyrics/tonybennett/solongbigtime.html',
 'http://www.azlyrics.com/lyrics/tonybennett/solitude.html',
 'http://www.azlyrics.com/lyrics/tonybennett/someotherspring.html',
 'http://www.azlyrics.com/lyrics/tonybennett/someothertime.html',
 'http://www.azlyrics.com/lyrics/tonybennett/something.html',
 'http://www.azlyrics.com/lyrics/tonybennett/somethinginyoursmile.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sometimesimhappy.html',
 'http://www.azlyrics.com/lyrics/tonybennett/somewherealongtheline.html',
 'http://www.azlyrics.com/lyrics/tonybennett/somewhereovertherainbow.html',
 'http://www.azlyrics.com/lyrics/tonybennett/speaklow.html',
 'http://www.azlyrics.com/lyrics/tonybennett/springishere.html',
 'http://www.azlyrics.com/lyrics/tonybennett/stellabystarlight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/steppinoutwithmybaby.html',
 'http://www.azlyrics.com/lyrics/tonybennett/strangerinparadise.html',
 'http://www.azlyrics.com/lyrics/tonybennett/streetofdreams.html',
 'http://www.azlyrics.com/lyrics/tonybennett/suddenly.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sweetgeorgiefame.html',
 'http://www.azlyrics.com/lyrics/tonybennett/sweetlorraine.html',
 'http://www.azlyrics.com/lyrics/tonybennett/swingingonastar.html',
 'http://www.azlyrics.com/lyrics/tonybennett/takethemoment.html',
 'http://www.azlyrics.com/lyrics/tonybennett/takingachanceonlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/tangerine.html',
 'http://www.azlyrics.com/lyrics/tonybennett/teafortwo.html',
 'http://www.azlyrics.com/lyrics/tonybennett/tellheritssnowing.html',
 'http://www.azlyrics.com/lyrics/tonybennett/tenderisthenight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/tenderly.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thatnight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thatoledevilcalledlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thatsentertainment.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thebarenecessities.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thebeatofmyheart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thebestisyettocome.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theboulevardofbrokendreams.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thegentlerain.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thegirlilove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thegoodlife.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thehandsoftime.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theinchworm.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thelongandwindingroad.html',
 'http://www.azlyrics.com/lyrics/tonybennett/themomentoftruth.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theplayground.html',
 'http://www.azlyrics.com/lyrics/tonybennett/therighttolove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theriviera.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thesecondtimearound.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theshadowofyoursmile.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theshiningsea.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theskyscraperblues.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thesummerknows.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thetrolleysong.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theverythoughtofyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thewaythatifeel.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thewayyoulooktonight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/themefromvalleyofthedolls.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thenwasthenandnowisnow.html',
 'http://www.azlyrics.com/lyrics/tonybennett/therewillneverbeanotheryou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/therellbesomechangesmade.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theresalullinmylife.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theresasmallhotel.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thesefoolishthingsremindmeofyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/theycanttakethatawayfromme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thinkhowitsgonnabe.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thiscantbelove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thisfunnyworld.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thisisalliask.html',
 'http://www.azlyrics.com/lyrics/tonybennett/thouswell.html',
 'http://www.azlyrics.com/lyrics/tonybennett/timetosmile.html',
 'http://www.azlyrics.com/lyrics/tonybennett/toottoottootsiegoodbye.html',
 'http://www.azlyrics.com/lyrics/tonybennett/tophatwhitetieandtails.html',
 'http://www.azlyrics.com/lyrics/tonybennett/touchofyourlips.html',
 'http://www.azlyrics.com/lyrics/tonybennett/touchtheearth.html',
 'http://www.azlyrics.com/lyrics/tonybennett/trappedinaweboflove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/travlinlight.html',
 'http://www.azlyrics.com/lyrics/tonybennett/tricks.html',
 'http://www.azlyrics.com/lyrics/tonybennett/truebluelou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/twilightworld.html',
 'http://www.azlyrics.com/lyrics/tonybennett/twobytwo.html',
 'http://www.azlyrics.com/lyrics/tonybennett/waittillyouseeher.html',
 'http://www.azlyrics.com/lyrics/tonybennett/walkabout.html',
 'http://www.azlyrics.com/lyrics/tonybennett/waltzfordebby.html',
 'http://www.azlyrics.com/lyrics/tonybennett/watchwhathappens.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wave.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wemustntsaygoodbye.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wellbetogetheragain.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whatalittlemoonlightcando.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whatawonderfulworld.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whatmakesithappen.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whattheworldneedsnowislove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whenawomanlovesaman.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whendothebellsringforme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whenifallinlove.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whenilookinyoureyes.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wheninrome.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whenlightsarelow.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whenyouwishuponastar.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wheredoyoustart.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whereorwhen.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whitechristmas.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whocaniturnto.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whocaniturntowhennobodyneedsme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whocares.html',
 'http://www.azlyrics.com/lyrics/tonybennett/whoeveryouareiloveyou.html',
 'http://www.azlyrics.com/lyrics/tonybennett/willowweepforme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/withoutasong.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wonderfulone.html',
 'http://www.azlyrics.com/lyrics/tonybennett/wrapyourtroublesindreamsanddreamyourtroublesaway.html',
 'http://www.azlyrics.com/lyrics/tonybennett/yellowdays.html',
 'http://www.azlyrics.com/lyrics/tonybennett/yesterdayiheardtherain.html',
 'http://www.azlyrics.com/lyrics/tonybennett/yesterdays.html',
 'http://www.azlyrics.com/lyrics/tonybennett/youdontknowwhatloveis.html',
 'http://www.azlyrics.com/lyrics/tonybennett/youshowedmetheway.html',
 'http://www.azlyrics.com/lyrics/tonybennett/youtookadvantageofme.html',
 'http://www.azlyrics.com/lyrics/tonybennett/yourealltheworldtome.html',
 'http://www.azlyrics.com/lyrics/tonybennett/youreeasytodancewithchangepartnerscheektocheek.html',
 'http://www.azlyrics.com/lyrics/tonybennett/youngandfoolish.html']

'''
