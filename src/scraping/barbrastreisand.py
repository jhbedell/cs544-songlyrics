title_stop_list = """Jingle Bells
Have Yourself A Merry Little Christmas
The Christmas Song (Chestnuts Roasting On An Open Fire)
White Christmas
My Favorite Things
The Best Gift
Sleep In Heavenly Peace (Silent Night)
Ave Maria
O Little Town Of Bethlehem
I Wonder As I Wander
Just Leave Everything To Me - Prologue - Call On Dolly
It Takes A Woman
It Takes A Woman (Reprise)
Put On Your Sunday Clothes
Ribbons Down My Back
Dancing
Before The Parade Passes By
Elegance
Love Is Only Love
Hello, Dolly!
It Only Takes A Moment
So Long Dearie
Hello, Dolly! Finale
Hurry, It's Lovely Up Here
Love With All The Trimmings
Melinda
Go To Sleep
He Isn't You
What Did I Have That I Don't Have?
Come Back To Me
On A Clear Day (You Can See Forever)
On A Clear Day (You Can See Forever) (Reprise)
How Lucky Can You Get
So Long Honey Lamb
I Found A Million Dollar Baby (In A Five & Ten Cent Store)
Isn't This Better
If I Love Again
I Got A Code In My Doze
(It's Gonna Be) A Great Day
Blind Date
Am I Blue?
It's Only A Paper Moon / I Like Him
More Than You Know
Let's Hear It For Me (Unreleased Intro)
Let's Hear It For Me
The Main Event / Fight
The Main Event (Ballad)
Where Is It Written?
Papa, Can You Hear Me?
This Is One Of Those Moments
No Wonder
The Way He Makes Me Feel
No Wonder (Part Two)
Tomorrow Night
Will Someone Ever Look At Me That Way?
No Matter What Happens
No Wonder (Reprise)
A Piece Of Sky
Putting It Together
If I Loved You
Something's Coming
Not While I'm Around
Being Alive
I Have Dreamed / We Kiss In A Shadow / Something Wonderful
Adelaide's Lament
Send In The Clowns
Pretty Women / The Ladies Who Lunch
I Loves You Porgy / Porgy I's Your Woman Now
Somewhere"""

url_stop_list = """http://www.azlyrics.com/lyrics/barbrastreisand/comerainorcomeshine380322.html
http://www.azlyrics.com/lyrics/barbrastreisand/dontrainonmyparade141283.html
http://www.azlyrics.com/lyrics/barbrastreisand/ithadtobeyou380320.html
http://www.azlyrics.com/lyrics/barbrastreisand/lostinsideofyou380332.html
http://www.azlyrics.com/lyrics/barbrastreisand/newyorkstateofmind380324.html
http://www.azlyrics.com/lyrics/barbrastreisand/themusicthatmakesmedance141089.html
http://www.azlyrics.com/lyrics/barbrastreisand/thewaywewere380326.html
http://www.azlyrics.com/lyrics/barbrastreisand/whatkindoffool380329.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
songlist = []
source = ""
with open("barbrastreisand.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("barbrastreisand1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Have to stop and restart it with new IP address after ~80-90 requests.
# First time running it, got to 122: "I Can See It"

with open("barbrastreisand2.lyr", 'w') as fout:
  for i in range(122,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 283: "Second Hand Rose"

with open("barbrastreisand3.lyr", 'w') as fout:
  for i in range(283,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 363: "In Rome (I Do As The Romans Do)"

with open("barbrastreisand4.lyr", 'w') as fout:
  for i in range(363,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Complete!
