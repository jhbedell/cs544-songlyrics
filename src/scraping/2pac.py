title_stop_list = """Days Of A Criminal
Dopefiend's Diner
Got My Mind Made Up (Remix)
I'm A Soldier
If I Die Young
If They Love Their Kidz
Life So Hard
Lost Souls
Made Niggaz
Never Had A Friend Like Me
Pain
Ride For Me
Road To Glory
The Death Of A True Thug
The Government
Watch Ya Mouth
What'z Ya Phone # (Remix)"""

url_stop_list = """http://www.azlyrics.com/lyrics/2pac/allboutu.html
http://www.azlyrics.com/lyrics/2pac/acrookedniggatooraphaelsaadiqremix.html
http://www.azlyrics.com/lyrics/2pac/fairxchangeremix.html
http://www.azlyrics.com/lyrics/2pac/hennesseyredspydaremix.html
http://www.azlyrics.com/lyrics/2pac/letemhaveitremix.html
http://www.azlyrics.com/lyrics/2pac/loyaltothegamedjquikremix.html
http://www.azlyrics.com/lyrics/2pac/myblockremix.html
http://www.azlyrics.com/lyrics/2pac/niggaznatureremix.html
http://www.azlyrics.com/lyrics/2pac/pacsliferemix.html
http://www.azlyrics.com/lyrics/2pac/poniggabluesscottstorchremix.html
http://www.azlyrics.com/lyrics/2pac/thugnuthugnmeremix.html
http://www.azlyrics.com/lyrics/2pac/untiltheendoftimerpremix.html
http://www.azlyrics.com/lyrics/2pac/untouchableswissbeatzremix.html
http://www.azlyrics.com/lyrics/2pac/iwonderifheavengotaghettohiphopversion.html
http://www.azlyrics.com/lyrics/2pac/thugzmansionnasacoustic.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("2pac.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("2pac1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 148: "Shit Don't Stop"
# grumpy disconnect

with open("2pac2.lyr", 'w') as fout:
  for i in range(148+1,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\n")
    end_lyrics = source.index("\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')



