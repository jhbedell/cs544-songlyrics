'''
Artist page is like:
http://www.azlyrics.com/s/springsteen.html
http://www.azlyrics.com/b/beatles.html

If on artist page, there is:
var songlist = [
{s:"1822!", h:"../lyrics/beatles/1822.html", c:"", a:""},
{s:"A Day In The Life", h:"../lyrics/beatles/adayinthelife.html", c:"", a:""},
{s:"A Hard Day's Night", h:"../lyrics/beatles/aharddaysnight.html", c:"", a:""},
{s:"A Little Rhyme", h:"../lyrics/beatles/alittlerhyme.html", c:"", a:""},
...
{s:"Your Mother Should Know", h:"../lyrics/beatles/yourmothershouldknow.html", c:"", a:""}];
]

Then each page is like:
http://www.azlyrics.com/lyrics/beatles/1822.html
'''

import requests, re



################################
#### Process File (Artists) ####
################################
list_of_artists = []
list_of_artist_urls = []
rex_artists = re.compile("([\w| |-]+)\thttp://www.azlyrics.com/(\w+/\w+.html)")
# artist_URL.data has each line as 'Band Name - url'

with open("artist_URL.data", 'r') as fin:
  for line in fin:
    result = re.findall(rex_artists, line)[0]
    list_of_artists.append(result[0])
    list_of_artist_urls.append(result[1])

# I decided to just download the .html files because I kept timing out and work with length of song lists



###################################
#### Process File (Stop Songs) ####
###################################
stop_songs = set([])
# list of song titles to ignore, e.g. Christmas songs
with open("stop_songs.data", 'r') as fin:
  for line in fin:
    stop_songs.add(line[:-1])




################################
####  Process URL (Artist)  ####
################################
html_files = []
with open("html.data", 'r') as fin:
  for line in fin:
    html_files.append(line[:-1])

songlists = []
for artist_file in html_files:
  source = ""
  with open(artist_file, 'r') as fin:
    for line in fin:
      source += line

  get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
  cur_songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')
  songlists.append(cur_songlist)


#### TO CHECK ARTISTS WITH COUNTS:

#for i in range(len(songlists)):
#    print(str(i) + '\t' + songlists[i][0][songlists[i][0].index('h:"..')+len('h:"../lyrics/'):songlists[i][0].index(".html")]+ '\t' + str(len(songlists[i])))



##################################
for artist_url in list_of_artist_urls:
  artist_page = requests.get(artist_url)
  artist_source = artist_page.text

  # Given an artist's page source, find the songlist
  get_songlist_rex = re.compile('var songlist = \[\\\r\\\n(\{[\w|\W]*\})\];')
  artist_songlist = re.search(get_songlist_rex, artist_source).groups()[0].split(',\n')

  # NOTE: gave up on making an ideal regex when I realized I was screwing it up 
  #       and it would actually be faster for the sake of the project to just do
  #       this part with iterations... will try and fix once I send the data to
  #       the next person in the group
  #  get_songlist_rex = re.compile('var songlist = \[\\\r\\\n(\{s:"[\w|\W]*", h:"[\w|\W]*", c:"[\w|\W]*", a:"[\w|\W]*"\}(,\n)?)+\];')

  # Parse out this songlist into song titles and song URLs
  rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
  for song in artist_songlist:
    result = re.findall(rex_songs, song)[0]
    song_title = result[0]
    if song_title not in stop_songs: # make sure it's not one of those Christmas songs or something
      song_url = http://www.azlyrics.com + result[1][2:]
      artists_song_titles.append(song_title)
      artists_song_urls.append(song_url)


#################
# find shortest song URL
#################
h = 4000

for i in range(len(song_urls)):
  if len(song_urls[i])<=h:
    h = len(song_urls[i])
    print(i)

#################
# Look for repeats
#################
for elt in song_urls:
  cur = elt[:51]
  if cur[:51]==last[:51]:
    print(elt[47:] + '\t' + last[47:])
  last = elt

# gotta change the #'s based on length; e.g. for Sinatra,
# we know there is only one copy of "if.html", but 
# the next shortest is "dindi.html", so we do 5char checks
# and his base URL ends at char 44, so we care about
# 44 and the 44+len(dindi) = 49
for elt in song_urls:
  cur = elt[:49]
  if cur[:49]==last[:49]:
    print(elt[44:] + '\t' + last[44:])
  last = elt

for elt in song_urls:
  cur = elt[:46]
  if cur[:46]==last[:46]:
    print(elt[42:] + '\t' + last[42:])
  last = elt

# t swift
for elt in song_urls:
  cur = elt[:46]
  if cur[:46]==last[:46]:
    print(elt[43:] + '\t' + last[43:])
  last = elt

# who
for elt in song_urls:
  cur = elt[:39]
  if cur[:39]==last[:39]:
    print(elt[35:] + '\t' + last[35:])
  last = elt

# skipped lil b

# mj
for elt in song_urls:
  cur = elt[:48]
  if cur[:48]==last[:48]:
    print(elt[46:] + '\t' + last[46:])
  last = elt

# rhcp
for elt in song_urls:
  cur = elt[:54]
  if cur[:54]==last[:54]:
    print(elt[50:] + '\t' + last[50:])
  last = elt

# aerosmith
for elt in song_urls:
  cur = elt[:45]
  if cur[:45]==last[:45]:
    print(elt[41:] + '\t' + last[41:])
  last = elt

# 2pac
for elt in song_urls:
  cur = elt[:39]
  if cur[:39]==last[:39]:
    print(elt[36:] + '\t' + last[36:])
  last = elt

# beegees
for elt in song_urls:
  cur = elt[:44]
  if cur[:44]==last[:44]:
    print(elt[39:] + '\t' + last[39:])
  last = elt

# jayz
for elt in song_urls:
  cur = elt[:41]
  if cur[:41]==last[:41]:
    print(elt[36:] + '\t' + last[36:])
  last = elt

# artist (length of initial URL, length of shortest checked song)
# diana ross (41, 41+5), beach boys (41, 41+5)
for elt in song_urls:
  cur = elt[:46]
  if cur[:46]==last[:46]:
    print(elt[41:] + '\t' + last[41:])
  last = elt

# rolling stones (45, 45+5
for elt in song_urls:
  cur = elt[:50]
  if cur[:50]==last[:50]:
    print(elt[45:] + '\t' + last[45:])
  last = elt

# dolly parton (43, 43+4)
for elt in song_urls:
  cur = elt[:47]
  if cur[:47]==last[:47]:
    print(elt[43:] + '\t' + last[43:])
  last = elt

################################
####   Process URL (Song)   ####
################################
with open(artist+".lyr", 'w') as fout:
  for song_url in artists_song_urls:
    fout.write('\n-----\n' + song_url + '\n')
    page = requests.get(song_url)
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    for line in lyrics:
      fout.write(line + '\n')


word_counts = []
for artist in list_of_artists:
  word_count = 0
  #process page
  for line in page:
#    word_count += len(line.split) # also check if line is "verse" or something...
    for word in line.split():
      word_count += 1
  word_counts.append(word_count)




###################################
# If this breaks, here's some BS4 #
###################################

# Got the idea to use requests from http://engineering.radius.com/post/57075964666/how-to-write-a-basic-web-crawler-in-20-lines-or
# (Already knew I wanted to use BeautifulSoup, but I initially planned on using urllib with it)


#  soup = BeautifulSoup(page.text)

#  pretty = soup.prettify()
#  begin_lyrics = pretty.index("<!-- start of lyrics -->")
#  end_lyrics = pretty.index("<!-- end of lyrics -->")

#  lyrics = BeautifulSoup(soup.prettify()[begin_lyrics+24:end_lyrics]) #+29? Both work for "Touch Me" by Willie Nelson
  
#soup.prettify()[soup.prettify().index("<!-- start of lyrics -->"):soup.prettify().index("<!-- end of lyrics -->")]

