title_stop_list = """"""
url_stop_list = """http://www.azlyrics.com/lyrics/frankzappa/motherlylove190298.html
http://www.azlyrics.com/lyrics/frankzappa/nonotnow189903.html
http://www.azlyrics.com/lyrics/frankzappa/plasticpeople190299.html
http://www.azlyrics.com/lyrics/frankzappa/youarewhatyouis189920.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("frankzappa.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("frankzappa1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 118: Deseri
# then quit out passive aggressively...

with open("frankzappa2.lyr", 'w') as fout:
  for i in range(118,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 171: "Gross Man"

with open("frankzappa3.lyr", 'w') as fout:
  for i in range(171,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 269: "N-Lite"
# then quit out passive aggressively...

with open("frankzappa4.lyr", 'w') as fout:
  for i in range(269,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')


# Got to 337: "Suicide Chump"

with open("frankzappa5.lyr", 'w') as fout:
  for i in range(337,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Got to 441: "Who Needs The Peace Corps?"

with open("frankzappa6.lyr", 'w') as fout:
  for i in range(441,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

