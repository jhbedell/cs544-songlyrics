title_stop_list = """Let It Snow! Let It Snow! Let It Snow!
Mistletoe And Holly
The First Noel
Baby It's Cold Outside
I Believe
Silver Bells
The Christmas Song
Hark! The Herald Angels Sing
Rudolph The Red-Nosed Reindeer
The Christmas Waltz
I've Got My Love To Keep Me Warm
Have Yourself A Merry Little Christmas
Silent Night
Jingle Bells
White Christmas
It Came Upon A Midnight Clear
Winter Wonderland
I'll Be Home For Christmas
Marshmallow World
The Twelve Days Of Christmas
The Bells Of Christmas (Greensleeves)"""

url_stop_list = """
"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("franksinatra.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("franksinatra1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    sleep(10)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

#### Disconnected at 49: "Baubles, Bangles And Beads"
####   got impatient and decreased wait time by half

with open("franksinatra2.lyr", 'w') as fout:
  for i in range(49,len(song_urls)):
    sleep(5)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

#### Disconnected at 121: "Everybody's Twistin'"
#### (forced off for too many requests)
#### trying longer wait times again, but not equally spaced

with open("franksinatra3.lyr", 'w') as fout:
  for i in range(121,len(song_urls)):
    sleep(2)
    if i%5==1:
      sleep(15)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

#### Disconnected at 215: "I Wish You Love"
#### regular disconnect!
#### going to stick with above wait times from now on :)

with open("franksinatra4.lyr", 'w') as fout:
  for i in range(215,len(song_urls)):
    sleep(10) # Kept getting yelled at again, so tried a few different sleep patterns
#    if i%5==1:
#      sleep(15)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

#### Disconnected at 333: "Misty"
#### grumpy disconnect :<

with open("franksinatra5.lyr", 'w') as fout:
  for i in range(333,len(song_urls)):
    sleep(10)
    if i%5==1: # trying both to see how far this can get...
      sleep(15)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

#### Disconnected at 516: "The Moon Was Yellow (And The Night Was Young)"
#### Timeout disconnect; thought it was grumpy, checked, not!

with open("franksinatra6.lyr", 'w') as fout:
  for i in range(516,len(song_urls)):
    sleep(3)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

#### Disconnected at 609: "Wives And Lovers"
#### Grumpy disconnect. At this point, worth it to just switch IPs

with open("franksinatra7.lyr", 'w') as fout:
  for i in range(609,len(song_urls)):
    #sleep(3)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Complete!
