title_stop_list = """Mean Woman Blues
(Let Me Be Your) Teddy Bear
Loving You
Got A Lot O' Livin' To Do
Lonesome Cowboy
Hot Dog
Party
Blueberry Hill
True Love
Don't Leave Me Now
Have I Told You Lately That I Love You?
I Need You So
Santa Claus Is Back In Town
White Christmas
Here Comes Santa Claus (Right Down Santa Claus Lane)
I'll Be Home For Christmas
Blue Christmas
Santa Bring My Baby Back (To Me)
O Little Town Of Bethlehem
Silent Night
(There'll Be) Peace In The Valley (For Me)
I Believe
Take My Hand, Precious Lord
It Is No Secret (What God Can Do)
buy this CD or download MP3s at amazon.com!
King Creole
As Long As I Have You
Hard Headed Woman
Trouble
Dixieland Rock
Don't Ask Me Why
Lover Doll
Crawfish
Young Dreams
Steadfast, Loyal And True
New Orleans
Tonight Is So Right For Love
What's She Really Like
Frankfort Special
Wooden Heart (Muss I Denn)
G.I. Blues
Pocketful Of Rainbows
Shoppin' Around
Big Boots
Didja' Ever
Blue Suede Shoes
Doin' The Best I Can
Blue Hawaii
Almost Always True
Aloha Oe
No More
Can't Help Falling In Love
Rock-A-Hula Baby
Moonlight Swim
Ku-U-I-Po
Ito Eats
Slicin' Sand
Hawaiian Sunset
Beach Boy Blues
Island Of Love
Hawaiian Wedding Song
Girls! Girls! Girls!
I Don't Wanna Be Tied
Where Do You Come From
I Don't Want To
We'll Be Together
A Boy Like Me, A Girl Like You
Earth Boy
Return To Sender
Because Of Love
Thanks To The Rolling Sea
Song Of The Shrimp
The Walls Have Ears
We're Comin' In Loaded
Beyond The Bend
Relax
Take Me To The Fair
They Remind Me Too Much Of You
One Broken Heart For Sale
I'm Falling In Love Tonight
Cotton Candy Land
A World Of Our Own
How Would You Like To Be?
Happy Ending
Fun In Acapulco
Vino, Dinero Y Amor
Mexico
El Toro
Marguerita
The Bullfighter Was A Lady
No Room To Rhumba In A Sports Car
I Think I'm Gonna Like It Here
Bossa Nova Baby
You Can't Say No In Acapulco
Guadalajara
Love Me Tonight
Slowly But Surely
Kissin' Cousins (No. 2)
Smokey Mountain Boy
There's Gold In The Mountains
One Boy, Two Little Girls
Catchin' On Fast
Tender Feeling
Anyone ( Could Fall In Love With You )
Barefoot Ballad
Once Is Enough
Kissin' Cousins
Echoes Of Love
(It's A) Long Lonely Highway
Roustabout
Little Egypt
Poison Ivy League
Hard Knocks
It's A Wonderful World
Big Love, Big Heartache
One Track Heart
It's Carnival Time
Carny Town
There's A Brand New Day On The Horizon
Wheels On My Heels
Girl Happy
Spring Fever
Fort Lauderdale Chamber Of Commerce
Startin' Tonight
Wolf Call
Do Not Disturb
Cross My Heart And Hope To Die
The Meanest Girl In Town
Do The Clam
Puppet On A String
I've Got To Find My Baby
You'll Be Gone
Harem Holiday
My Desert Serenade
Go East, Young Man
Mirage
Kismet
Shake That Tambourine
Hey Little Girl
Golden Coins
So Close, Yet So Far
Animal Instinct
Wisdom Of The Ages
Frankie And Johnny
Come Along
Petunia The Gardener's Daughter
Chesay
What Every Woman Lives For
Look Out Broadway
Beginner's Luck
Down By The Riverside
When The Saints Go Marchin' In
Shout It Out
Hard Luck
Please Don't Stop Loving Me
Everybody Come Aboard
Paradise, Hawaiian Style
Queenie Wahine's Papaya
Scratch My Back
Drums Of The Islands
Datin'
A Dog's Life
House Of Sand
Stop Where You Are
This Is My Heaven
Sand Castles
Stop Look And Listen
Adam And Evil
All That I Am
Never Say Yes
Am I Ready
Beach Shack
Spinout
Smorgasbord
I'll Be Back
Tomorrow Is A Long Time
Down In The Alley
I'll Remember You
Double Trouble
Baby, If You'll Give Me All Of Your Love
Could I Fall In Love
Long Legged Girl (With The Short Dress On)
City By Night
Old MacDonald
I Love Only One Girl
There's So Much World To See
It Won't Be Long
Never Ending
Blue River
What Now, What Next, Where To
Guitar Man
Clambake
Who Needs Money
A House That Has Everything
Confidence
Hey, Hey, Hey
You Don't Know Me
The Girl I Never Loved
How Can You Lose What You Never Had?
Big Boss Man
Singing Tree
Just Call Me Lonesome
Speedway
There Ain't Nothing Like A Song
Your Time Hasn't Come Yet Baby
Who Are You (Who Am I?)
He's Your Uncle, Not Your Dad
Let Yourself Go
Five Sleepy Heads
Western Union
Mine
Going Home
Suppose
buy this CD or download MP3s at amazon.com!
I Just Can't Help Believin'
Twenty Days And Twenty Nights
How The Web Was Woven
Patch It Up
Mary In The Morning
You Don't Have To Say You Love Me
You've Lost That Lovin' Feelin'
I've Lost You
Just Pretend
Stranger In The Crowd
The Next Step Is Love
Bridge Over Troubled Water"""

url_stop_list = """http://www.azlyrics.com/lyrics/elvispresley/alittlelessconversationjxlradioeditremix.html
http://www.azlyrics.com/lyrics/elvispresley/rubberneckinpauloakenfoldremix.html"""

title_stops = set([elt for elt in title_stop_list.split('\n') if elt!=""])
url_stops = set([elt for elt in url_stop_list.split('\n') if elt!=""])

import requests, re
from time import sleep

songlist = []
source = ""
with open("elvis.html", 'r') as fin:
  for line in fin:
    source += line

get_songlist_rex = re.compile('var songlist = \[\\\n(\{[\w|\W]*\})\];')
songlist = re.search(get_songlist_rex, source).groups()[0].split(',\n')

rex_songs = re.compile('\{s:"([\w|\W]+)", h:"([\w|\W]+)", c:"[\w|\W]*", a:"[\w|\W]*"\}')
song_titles = []
song_urls = []
for song in songlist:
  result = re.findall(rex_songs, song)[0]
  song_title = result[0]
  song_url = "http://www.azlyrics.com" + result[1][2:]
  if song_title not in title_stops and song_url not in url_stops:
    song_titles.append(song_title)
    song_urls.append(song_url)

#######################################

with open("elvis1.lyr", 'w') as fout:
  for i in range(len(song_urls)):
    if i%5==1:
      sleep(15)
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')


# Was booted SILENTLY!! What a pain D:<
# First time running it, got to 36: "Blue Moon Of Kentucky"

with open("elvis2.lyr", 'w') as fout:
  for i in range(36,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Disconnected at 153: "I'm Coming Home"
# Grumpy disconnect

with open("elvis3.lyr", 'w') as fout:
  for i in range(153,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Disconnected at 276: "Promised Land"
# Grumpy disconnect? Passive aggressiveness? Everything is messed up!

with open("elvis4.lyr", 'w') as fout:
  for i in range(276,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')

# Disconnected at 374: "We Can Make The Morning"
# Grumpy disconnect

with open("elvis5.lyr", 'w') as fout:
  for i in range(374,len(song_urls)):
    title = song_titles[i]
    url = song_urls[i]

    page = requests.get(song_urls[i])
    source = page.text
    begin_lyrics = source.index("<!-- start of lyrics -->") + len("<!-- start of lyrics -->\r\n")
    end_lyrics = source.index("\r\n<!-- end of lyrics -->")

    text = source[begin_lyrics:end_lyrics].replace("<br />", "")
    lyrics = text.split('\n')

    fout.write('\n-----\n' + str(i) + '\t' + title + '\t' + url + '\n')
    for line in lyrics:
      fout.write(line + '\n')
