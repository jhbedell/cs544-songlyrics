# Author: Ayush Jaiswal

from __future__ import division
from collections import defaultdict
from reader import Reader, Song
import featureKeys
import paths
import nltk
import pickle
import re

class FeatureExtractor:
    '''Extracts features from the lyrics of a given song.'''

    def __init__(self):
        self.__pronunciationDictionary = self.__readPronunciationDictionary()

    def __readPronunciationDictionary(self):
        '''Read pronunciation dictionary from file and return as a dictionary.'''
        pronunciationDictionary = {}
        with open(paths.pronunciationDictionaryPath) as f:
            x = f.read()
            lines = [line for line in x.split('\n') if (line != '' and line[0] != ';')]
            for line in lines:
                word, pronunciation = line.split('  ')
                pronunciationDictionary[word] = pronunciation.split()
        return pronunciationDictionary

    def __getSimilarity(self, set1, set2):
        '''Generate similarity score as overlap.'''
        vocabulary = set1.union(set2)
        common = set1.intersection(set2)
        return (len(common) / len(vocabulary))

    def __getStanzas(self, lyrics):
        '''Returns a list of stanzas from the lyrics.'''
        stanzas = [s + '\n' for s in lyrics.split('\n\n')]
        stanzas[-1] = stanzas[-1][:-1]
        return stanzas

    def extractSongStructure(self, lyrics):
        '''Extracts song structure from the lyrics in terms of V(erse)/C(horus)<n>/L(ine)/R(epeated line)/#(start)/$(end).'''
        stanzas = self.__getStanzas(lyrics)
        stanzasBOW = [set([word.lower() for word in stanza.split()]) for stanza in stanzas]
        nStanzas = len(stanzas)
        if nStanzas == 1:
            return stanzas, ['#', 'V', '$'], [0, stanzas[0].count('\n'), 0]
        structure = ['V' for i in range(nStanzas)]
        chorusIdx = 1
        for i in range(nStanzas):
            if stanzas[i].count('\n') == 1:
                structure[i] = ('L')
            elif structure[i] == 'V':
                foundChorus = False
                for j in range(i + 1, nStanzas):
                    if stanzas[j].count('\n') == 1:
                        continue
                    similarityScore = self.__getSimilarity(stanzasBOW[i], stanzasBOW[j])
                    if similarityScore >= 0.7:
                        structure[j] = 'C%d' % chorusIdx
                        foundChorus = True
                if foundChorus:
                    structure[i] = 'C%d' % chorusIdx
                    chorusIdx = chorusIdx + 1
                elif len(set(stanzas[i].split())) == 1:
                    structure[i] = 'R'
        return stanzas, ['#'] + structure + ['$'], [0] + [stanza.count('\n') for stanza in stanzas] + [0]

    def __preprocessLyrics(self, lyrics):
        '''Returns list A (B) of lines in the lyrics, each line being a list of words (tags).'''
        lines = [nltk.word_tokenize(line) for line in lyrics.split('\n') if line != '']
        tags = [[tag for word, tag in nltk.pos_tag(line)] for line in lines]
        return lines, tags

    def __extractOrthographicFeatures(self, songFeatures, lyrics, stanzas, structure, tokenizedLines, tags, words):
        '''Extracts orthographic features from the lyrics of the song.'''
        # Adding macro features
        songFeatures[featureKeys.wordCount] = len(words)
        songFeatures[featureKeys.lineCount] = len(tokenizedLines)
        songFeatures[featureKeys.stanzaCount] = len(stanzas)
        songFeatures[featureKeys.VCount] = structure.count('V')
        songFeatures[featureKeys.RCount] = structure.count('R')
        songFeatures[featureKeys.LCount] = structure.count('L')
        songFeatures[featureKeys.CCount] = len([t for t in structure if t[0] == 'C'])
        wordLengths = [len(word) for word in words]
        songFeatures[featureKeys.avgWordLength] = sum(wordLengths) / len(wordLengths)
        # Calculating and adding structure related features
        stanzaLineCountsV = []
        stanzaLineCountsC = []
        stanzaLineCountsR = []
        stanzaLineCountsL = []
        lineLengthsV = []
        lineLengthsC = []
        lineLengthsR = []
        lineLengthsL = []
        for i in range(len(stanzas)):
            stanza = stanzas[i]
            lines = stanza.split('\n')
            if lines[-1] == '':
                lines = lines[:-1]
            lineLength = [len(line.split()) for line in lines]
            if structure[i + 1] == 'V':
                lineLengthsV = lineLengthsV + lineLength
                stanzaLineCountsV.append(len(lines))
            elif structure[i + 1][0] == 'C':
                lineLengthsC = lineLengthsC + lineLength
                stanzaLineCountsC.append(len(lines))
            elif structure[i + 1] == 'R':
                lineLengthsR = lineLengthsR + lineLength
                stanzaLineCountsR.append(len(lines))
            else:
                lineLengthsL = lineLengthsL + lineLength
                stanzaLineCountsL.append(len(lines))
        if len(stanzaLineCountsV) > 0:
            songFeatures[featureKeys.avgStanzaLengthV] = sum(stanzaLineCountsV) / len(stanzaLineCountsV)
        if len(stanzaLineCountsC) > 0:
            songFeatures[featureKeys.avgStanzaLengthC] = sum(stanzaLineCountsC) / len(stanzaLineCountsC)
        if len(stanzaLineCountsR) > 0:
            songFeatures[featureKeys.avgStanzaLengthR] = sum(stanzaLineCountsR) / len(stanzaLineCountsR)
        if len(stanzaLineCountsL) > 0:
            songFeatures[featureKeys.avgStanzaLengthL] = sum(stanzaLineCountsL) / len(stanzaLineCountsL)
        if len(lineLengthsV) > 0:
            songFeatures[featureKeys.avgLineLengthV] = sum(lineLengthsV) / len(lineLengthsV)
        if len(lineLengthsC) > 0:
            songFeatures[featureKeys.avgLineLengthC] = sum(lineLengthsC) / len(lineLengthsC)
        if len(lineLengthsL) > 0:
            songFeatures[featureKeys.avgLineLengthL] = sum(lineLengthsL) / len(lineLengthsL)
        if len(lineLengthsR) > 0:
            songFeatures[featureKeys.avgLineLengthR] = sum(lineLengthsR) / len(lineLengthsR)
        # Calculating and adding frequencies for the most frequent noun, verb, and adjective
        taggedWordFrequencies = {'noun' : defaultdict(int), 'verb' : defaultdict(int), 'adjective' : defaultdict(int)}
        taggedWordsFlat = [word for line in tokenizedLines for word in line]
        tagsFlat = [tag for taggedLine in tags for tag in taggedLine]
        for i in range(len(tagsFlat)):
            word = taggedWordsFlat[i].lower()
            tag = tagsFlat[i]
            if tag[0] == 'N':
                taggedWordFrequencies['noun'][word] = taggedWordFrequencies['noun'][word] + 1
            elif tag[0] == 'V':
                taggedWordFrequencies['verb'][word] = taggedWordFrequencies['verb'][word] + 1
            elif tag[0] == 'J':
                taggedWordFrequencies['adjective'][word] = taggedWordFrequencies['adjective'][word] + 1
        nounsFound = taggedWordFrequencies['noun']
        verbsFound = taggedWordFrequencies['verb']
        adjectivesFound = taggedWordFrequencies['adjective']
        if len(nounsFound.keys()) > 0:
            songFeatures[featureKeys.largestFrequencyNoun] = max([nounsFound[word] for word in nounsFound.keys()])
        if len(verbsFound.keys()) > 0:
            songFeatures[featureKeys.largestFrequencyVerb] = max([verbsFound[word] for word in verbsFound.keys()])
        if len(adjectivesFound.keys()) > 0:
            songFeatures[featureKeys.largestFrequencyAdjective] = max([adjectivesFound[word] for word in adjectivesFound.keys()])
        # Percentage of bigrams that are exactly the same word repeated twice:
        count = 0
        nBigrams = len(words) - 1
        for i in range(nBigrams):
            if words[i] == words[i + 1]:
                count = count + 1
            elif words[i][-1] == ',':  # because lines are split into words by space naively
                if words[i + 1][-1] == ',':
                    if words[i][:-1] == words[i + 1][:-1]:
                        count = count + 1
                elif words[i][:-1] == words[i + 1]:
                    count = count + 1
        songFeatures[featureKeys.percBigram] = (count / nBigrams) * 100
        # Percentage of lines that are the same as the most frequent line
        linesUntokenized = [line for line in lyrics.split('\n') if line != '']
        lineFrequencies = defaultdict(int)
        for line in linesUntokenized:
            lineFrequencies[line] = lineFrequencies[line] + 1
        frequencies = [lineFrequencies[line] for line in lineFrequencies.keys()]
        songFeatures[featureKeys.percLine] = (max(frequencies) / sum(frequencies)) * 100
        # Percentage of stanzas that are the same as the most frequent stanza
        stanzaFrequencies = defaultdict(int)
        for stanza in stanzas:
            stanzaFrequencies[stanza] = stanzaFrequencies[stanza] + 1
        frequencies = [stanzaFrequencies[stanza] for stanza in stanzaFrequencies.keys()]
        songFeatures[featureKeys.percStanza] = (max(frequencies) / sum(frequencies)) * 100
        # Number of times the last line is repeated
        count = 1
        nLines = len(linesUntokenized)
        for i in range(nLines - 2, -1, -1):
            if linesUntokenized[i] != linesUntokenized[-1]:
                break
            count = count + 1
        songFeatures[featureKeys.numRepLastLine] = count
        # Number of times the last stanza is repeated
        count = 1
        nStanzas = len(stanzas)
        for i in range(nStanzas - 2, -1, -1):
            if stanzas[i] != stanzas[-1]:
                break
            count = count + 1
        songFeatures[featureKeys.numRepLastStanza] = count
        return songFeatures

    def __extractSyntacticFeatures(self, songFeatures, lyrics, tags):
        '''Extracts syntactic features from the lyrics of the song.'''
        for tag in tags:
            if tag.startswith('N'):
                songFeatures[featureKeys.freqNoun] = songFeatures[featureKeys.freqNoun] + 1
            elif tag.startswith('V'):
                songFeatures[featureKeys.freqVerb] = songFeatures[featureKeys.freqVerb] + 1
            elif tag.startswith('J'):
                songFeatures[featureKeys.freqAdjective] = songFeatures[featureKeys.freqAdjective] + 1
            elif tag.startswith('RB'):
                songFeatures[featureKeys.freqAdverb] = songFeatures[featureKeys.freqAdverb] + 1
            elif tag.startswith('PR'):
                songFeatures[featureKeys.freqPronoun] = songFeatures[featureKeys.freqPronoun] + 1
            elif tag.startswith('I'):
                songFeatures[featureKeys.freqPreposition] = songFeatures[featureKeys.freqPreposition] + 1
            elif tag.startswith('CC'):
                songFeatures[featureKeys.freqConjunction] = songFeatures[featureKeys.freqConjunction] + 1
            elif tag.startswith('U'):
                songFeatures[featureKeys.freqInterjection] = songFeatures[featureKeys.freqInterjection] + 1
        nTags = len(tags)
        songFeatures[featureKeys.freqNoun] = songFeatures[featureKeys.freqNoun] / nTags
        songFeatures[featureKeys.freqVerb] = songFeatures[featureKeys.freqVerb] / nTags
        songFeatures[featureKeys.freqAdjective] = songFeatures[featureKeys.freqAdjective] / nTags
        songFeatures[featureKeys.freqAdverb] = songFeatures[featureKeys.freqAdverb] / nTags
        songFeatures[featureKeys.freqPronoun] = songFeatures[featureKeys.freqPronoun] / nTags
        songFeatures[featureKeys.freqPreposition] = songFeatures[featureKeys.freqPreposition] / nTags
        songFeatures[featureKeys.freqConjunction] = songFeatures[featureKeys.freqConjunction] / nTags
        songFeatures[featureKeys.freqInterjection] = songFeatures[featureKeys.freqInterjection] / nTags
        return songFeatures

    def __containsAlpha(self, word):
        '''Checks whether the word has any alphabet.'''
        for c in word:
            if c.isalpha():
                return True
        return False

    def extractRhymeKey(self, pronunciation):
        '''Extracts part of the pronunciation that is important for rhyme check.'''
        rhymeKey = ''
        idx = len(pronunciation) - 1
        while idx >= 0 and not (pronunciation[idx][-1].isdigit() and int(pronunciation[idx][-1]) > 0):
            idx = idx - 1
        if idx < 0:
            idx = 0
        rhymeKey = '-'.join(pronunciation[idx:])
        rhymeKey = re.sub('\d', '', rhymeKey) 
        return rhymeKey

    def extractRhymeKeyFromWord(self, word):
        '''Extracts part of the pronunciation of the word that is important for rhyme check.'''
        word = word.upper()
        rhymeKey = ''
        try:
            pronunciation = self.__pronunciationDictionary[word]
            idx = len(pronunciation) - 1
            while idx >= 0 and not (pronunciation[idx][-1].isdigit() and int(pronunciation[idx][-1]) > 0):
                idx = idx - 1
            if idx < 0:
                idx = 0
            rhymeKey = '-'.join(pronunciation[idx:])
            rhymeKey = re.sub('\d', '', rhymeKey)
        except KeyError:
            rhymeKey = word
        return rhymeKey

    def __lastVowelIndex(self, word):
        '''Returns the index of the last vowel in the word.'''
        vowels = ['a', 'e', 'i', 'o', 'u']
        indices = [word.rfind(vowel) for vowel in vowels]
        l = len(word)
        idx = max(indices)
        if idx == -1:
            return l
        return idx

    def extractRhymeScheme(self, stanzas):
        '''Extracts rhyme scheme for every stanza of the lyrics.'''
        rhymeSchemeList = []
        for stanza in stanzas:
            lines = [line for line in stanza.split('\n') if line != '']
            rhymeScheme = [None for i in range(len(lines))]
            rhymeKeyList = [None for i in range(len(lines))]
            for x in range(len(lines)):
                line = lines[x]
                words = line.split()
                lastWordIdx = len(words) - 1
                while lastWordIdx >= 0 and not self.__containsAlpha(words[lastWordIdx]):
                    lastWordIdx = lastWordIdx - 1
                lastWordAux = words[lastWordIdx]
                l = len(lastWordAux) - 1
                while l >= 0 and not lastWordAux[l].isalpha():
                    l = l - 1
                i = l
                while i >= 0 and lastWordAux[i].isalpha():
                    i = i - 1
                lastWord = lastWordAux[(i + 1):(l + 1)]
                if len(lastWordAux) > 2 and l < len(lastWordAux) - 1 and lastWordAux[l-1] == 'i' and lastWordAux[l] == 'n' and lastWordAux[l+1] == "'":
                    lastWord = lastWord + 'g'
                rhyme = None
                try:
                    lastWordPronunciation = self.__pronunciationDictionary[lastWord.upper()]
                    rhymeKey = self.extractRhymeKey(lastWordPronunciation)
                    rhymeKeyList[x] = (lastWord.lower(), rhymeKey)
                except:
                    rhymeKeyList[x] = (lastWord.lower(), None)
            counter = 1
            for x in range(len(lines)):
                if rhymeScheme[x] is None:
                    newKey = False
                    word1, rhymeKey1 = rhymeKeyList[x]
                    for y in range(x + 1, len(lines)):
                        word2, rhymeKey2 = rhymeKeyList[y]
                        if word2 == word1:
                            rhymeScheme[y] = str(counter)
                            newKey = True
                        elif rhymeKey2 == rhymeKey1:
                            rhymeScheme[y] = str(counter)
                            newKey = True
                    if newKey:
                        rhymeScheme[x] = str(counter)
                        counter = counter + 1
            if None in rhymeScheme:
                endingIndices = {}
                for x in range(len(lines)):
                    word = rhymeKeyList[x][0]
                    ending = word[self.__lastVowelIndex(word):]
                    try:
                        endingIndices[ending].append(x)
                    except:
                        endingIndices[ending] = [x]
                for ending in endingIndices.keys():
                    r = None
                    for index in endingIndices[ending]:
                        if rhymeScheme[index] is not None:
                            r = rhymeScheme[index]
                            break
                    if len(ending) == 1:
                        r = 'N'
                    elif r is None:
                        r = str(counter)
                        counter = counter + 1
                    for index in endingIndices[ending]:
                        if rhymeScheme[index] is None:
                            rhymeScheme[index] = r
            counter = 1
            rhymeSchemeFormatted = [0 for k in rhymeScheme]
            for i in range(len(rhymeScheme)):
                if rhymeSchemeFormatted[i] == 0:
                    rhymeSchemeFormatted[i] = str(counter)
                    if rhymeScheme[i] != 'N':
                        for j in range(i + 1, len(rhymeScheme)):
                            if rhymeScheme[i] == rhymeScheme[j]:
                                rhymeSchemeFormatted[j] = str(counter)
                    counter = counter + 1
            rhymeSchemeList.append(rhymeSchemeFormatted)
        return rhymeSchemeList

    def extractFeatures(self, lyrics, stanzas, structure):
        '''Extracts orthographic and syntactic features from the lyrics of the song.'''
        lines, tags = self.__preprocessLyrics(lyrics)
        songFeatures = {feature : 0 for feature in featureKeys.featureKeysList}
        words = [word.lower() for word in lyrics.split()]
        songFeatures = self.__extractOrthographicFeatures(songFeatures, lyrics, stanzas, structure, lines, tags, words)
        tagsFlat = [tag for line in tags for tag in line]
        songFeatures = self.__extractSyntacticFeatures(songFeatures, lyrics, tagsFlat)
        return songFeatures

class ArtistProcessor:
    '''Extracts features from songs of an artist and represents them as a collection of feature vectors.'''

    def __init__(self, artistDirectory):
        self.artistDirectory = artistDirectory
        songsReader = Reader(artistDirectory)
        self.songs = songsReader.read()
        self.nSongs = len(self.songs)
        self.structuresList = []  # list of song structures
        self.featuresList = []  # list of song features
        self.rhymeSchemeList = []  # list of rhyme schemes of songs
        self.lengthsList = []  # list of lists of stanza lengths

    def process(self):
        '''Generates lists of structures and features of all the songs for the artist.'''
        featureExtractor = FeatureExtractor()
        for i in range(self.nSongs):
            print self.songs[i].name
            stanzas, structure, lengths = featureExtractor.extractSongStructure(self.songs[i].lyrics)
            self.structuresList.append(structure)
            self.lengthsList.append(lengths)
            songFeatures = featureExtractor.extractFeatures(self.songs[i].lyrics, stanzas, structure)
            self.featuresList.append(songFeatures)
            songRhymeScheme = featureExtractor.extractRhymeScheme(stanzas)
            self.rhymeSchemeList.append(songRhymeScheme)

if __name__ == '__main__':
    artistDirectoryRoot = '../../data/'
    dumpPath = '../../data/features/'
    artistFolderNames = ['2pac', 'aerosmith', 'barbrastreisand', 'beachboys', 'beegees', 'cash',
                        'dianaross', 'elvis', 'franksinatra', 'jackson', 'rollingstones', 'taylorswift',
                        'jamesbedell', 'jayz', 'languagemodels', 'pages', 'redhotchilipeppers',
                        'tonybennett', 'who', 'lilb']
    for artistFolderName in artistFolderNames:
        processor = ArtistProcessor(artistDirectoryRoot + artistFolderName)
        print 'Processing ' + artistFolderName + '...'
        processor.process()
        with open(dumpPath + artistFolderName + '.pkl', 'wb') as f:
            pickle.dump((processor.featuresList, processor.structuresList, processor.rhymeSchemeList, processor.lengthsList), f)
        print
