# Author: Ayush Jaiswal

from __future__ import division
import pickle
import numpy
from scipy import stats as ss
import featureKeys
from artistProcessor import FeatureExtractor

class Artist:
    '''Provides access to features extracted from songs of the artist and a reranking function for the artist.'''

    def __init__(self, artistFeatureDumpPath):
        self.__featuresList = None
        self.__structuresList = None
        self.__rhymeSchemeList = None
        with open(artistFeatureDumpPath, 'rb') as f:
            self.__featuresList, self.__structuresList, self.__rhymeSchemeList, self.__lengthsList = pickle.load(f)
        self.__feautureVectorsList = [self.getFeatureVector(featureDict) for featureDict in self.__featuresList]
        nSongs = len(self.__featuresList)
        nFeatures = len(self.__feautureVectorsList[0])
        self.__centroidFeatureVector = [0 for i in range(nFeatures)]
        for j in range(nFeatures):
            self.__centroidFeatureVector[j] = sum([self.__feautureVectorsList[i][j] for i in range(nSongs)]) / nSongs
        self.__featureExtractor = FeatureExtractor()

    def getFeaturesList(self):
        '''Returns orthographic and syntactic features extracted from songs of the artist.'''
        return self.__featuresList

    def getFeatureValues(self, key):
        '''Returns list of values for the feature requested through key (ideally a variable in featureKeys.py).'''
        return [featureDict[key] for featureDict in self.__featuresList]

    def getStructuresList(self):
        '''Returns a list of song structures extracted from songs of the artist.'''
        return self.__structuresList

    def getLengthsList(self):
        '''Returns a list of stanza lengths extracted from songs of the artist.'''
        return self.__lengthsList

    def getRhymeSchemeList(self):
        '''Returns a list of rhyme scheme lists (one for each stanza of each song) as extracted from songs of the artist.'''
        return self.__rhymeSchemeList

    def getFeatureVector(self, featureDict):
        '''Returns a vector representation of the feature dictionary for a song.'''
        return [featureDict[key] for key in featureKeys.featureKeysList]

    def getFeaturesForLyrics(self, generatedLyrics):
        '''Returns a dictionary of features for the generated lyrics.'''
        stanzas, structure, lengths = self.__featureExtractor.extractSongStructure(generatedLyrics)
        features = self.__featureExtractor.extractFeatures(generatedLyrics, stanzas, structure)
        return features

    def getRerankingScore(self, generatedLyrics):
        '''Returns reranking score for generated lyrics using distance from cluster formed by feature-vectors for the artist.'''
        # featuresOfGeneratedLyrics = self.getFeatureVector(self.getFeaturesForLyrics(generatedLyrics))
        featuresOfGeneratedLyrics = self.getFeaturesForLyrics(generatedLyrics)
        nFeatures = len(featuresOfGeneratedLyrics)
        totalscore = 0.0
        for feature in featureKeys.featureKeysList:
            fvals = self.getFeatureValues(feature)
            featureMean = numpy.mean(fvals)
            featureStdDev = numpy.std(fvals)
            thisFeature = featuresOfGeneratedLyrics[feature]
            if featureStdDev == 0:
                if thisFeature == featureMean:
                    thisFeatureScaled = 0.0
                else:
                    thisFeatureScaled = 10 # that's enough standard deviations to get the point home that it's weird
            else:
                thisFeatureScaled = (thisFeature - featureMean) / featureStdDev
            featurepenalty = numpy.log(ss.norm.pdf(thisFeatureScaled))
            totalscore += featurepenalty
        # score = sum([(featuresOfGeneratedLyrics[i] - self.__centroidFeatureVector[i]) ** 2 for i in range(nFeatures)])
        return -totalscore
