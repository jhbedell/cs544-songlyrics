# Author: Ayush Jaiswal

wordCount = 'word-count'
lineCount = 'line-count'
stanzaCount = 'stanza-count'
VCount = 'V-count'
CCount = 'C-count'
LCount = 'L-count'
RCount = 'R-count'
avgLineLengthV = 'avg-line-length-V'
avgLineLengthC = 'avg-line-length-C'
avgLineLengthR = 'avg-line-length-R'
avgLineLengthL = 'avg-line-length-L'
avgWordLength = 'avg-word-length'
avgStanzaLengthV = 'avg-stanza-length-V'  # number of lines
avgStanzaLengthC = 'avg-stanza-length-C'  # number of lines
avgStanzaLengthR = 'avg-stanza-length-R'  # number of lines
avgStanzaLengthL = 'avg-stanza-length-L'  # number of lines
largestFrequencyNoun = 'largest-frequency-noun'
largestFrequencyVerb = 'largest-frequency-verb'
largestFrequencyAdjective = 'largest-frequency-adjective'
percBigram = 'perc-bigram'  # % of bigrams that are exactly the same word repeated
percLine = 'perc-line'  # % of lines in the song that are the same as the most frequent line
percStanza = 'perc-stanza'  # % of stanzas that are the same as the most frequent stanza
numRepLastLine = 'num-rep-last-line'  # Largest n such that the last n lines of the song are all the same
numRepLastStanza = 'num-rep-last-stanza'  # Largest n such that the last n stanzas of the song are all the same
freqNoun = 'freq-noun'
freqVerb = 'freq-verb'
freqAdjective = 'freq-adjective'
freqAdverb = 'freq-adverb'
freqPronoun = 'freq-pronoun'
freqPreposition = 'freq-preposition'
freqConjunction = 'freq-conjunction'
freqInterjection = 'freq-interjection'
featureKeysList = [wordCount, lineCount, stanzaCount, VCount, CCount, LCount, RCount,
                   avgLineLengthV, avgLineLengthC, avgLineLengthR, avgLineLengthL,
                   avgWordLength, avgStanzaLengthV, avgStanzaLengthC, avgStanzaLengthR, avgStanzaLengthL,
                   largestFrequencyNoun, largestFrequencyVerb,
                   largestFrequencyAdjective, percBigram, percLine, percStanza, numRepLastLine,
                   numRepLastStanza, freqNoun, freqVerb, freqAdjective, freqAdverb, freqPronoun, freqPreposition,
                   freqConjunction, freqInterjection]