# Author: Ayush Jaiswal

import os
import re

class Reader:
    '''Object of this class can be used to read song lyrics from .lyr files.'''

    def __init__(self, artistDirectory):
        self.artistDirectory = artistDirectory
        if self.artistDirectory[-1] != '/':
            self.artistDirectory = self.artistDirectory + '/'

    def __parse(self, songSection):
        '''Parse content for each song and extract name, url, and lyrics.'''
        metaLineEnd = songSection.index('\n')
        meta = songSection[:metaLineEnd]
        lyrics = songSection[metaLineEnd + 1:]
        idx, name, url = meta.strip().split('\t')
        return name, url, lyrics

    def __clean(self, lyrics):
        '''Cleans the lyrics by removing meta-data in <i></i>.'''
        lyrics = lyrics.replace('\r', '')
        lines = [line.strip() for line in lyrics.split('\n') if '<i>' not in line]
        cleanedLyrics = '\n'.join(lines)
        nonEmptyLines = [line for line in lines if line != '']
        if len(nonEmptyLines) == 0:
            return None
        return cleanedLyrics

    def read(self):
        '''Reads song lyrics from .lyr files and returns a list of Song objects.'''
        songs = []
        files = os.listdir(self.artistDirectory)
        for f in files:
            if f[0] == '.':
                continue
            contents = ''
            with open(self.artistDirectory + f) as fIn:
                contents = fIn.read()
            contents = contents.replace('\xc3\xa2\xc2\x80\xc2\x99', '\'')
            contents = contents.replace('\r', '')
            songSections = re.split('\n-----+\n', contents)[1:]
            for songSection in songSections:
                name, url, lyrics = self.__parse(songSection)
                lyrics = self.__clean(lyrics)
                if lyrics is not None:
                    songs.append(Song(name, url, lyrics))
        return songs

class Song:
    '''Data structure for each song.'''

    def __init__(self, name, url, lyrics):
        self.name = name
        self.url = url
        self.lyrics = lyrics