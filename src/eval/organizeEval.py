artists = set(["Aerosmith", "The Beach Boys", "Barbra Streisand", "Bee Gees", "Diana Ross", "Elvis Presley", "Frank Sinatra", "Johnny Cash", "Lil B", "Michael Jackson", "Red Hot Chili Peppers", "The Rolling Stones", "Taylor Swift", "Tony Bennett", "The Who"])

hold = []
with open("eval_counts.txt", 'r') as fin:
  for line in fin:
    hold.append(line.replace('\n', ""))


# Read in dictionary of PARTICIPANT => [list, of, artists]
from collections import defaultdict
p_dict = defaultdict(list)

for line in hold:
  line = line.split('\t')
  p_dict[line[0]] = line[1:]

# count how many times each artist occurs
total_counts = defaultdict(int)
actual_counts = defaultdict(int)
generated_counts = defaultdict(int)

generated_artists_
for all_artists in p_dict.values():
  for artist in all_artists:
    if "(" in artist:
      total_counts[artist[:artist.index("(")-1]] += 1
      if "(A)" in artist:
        actual_counts[artist[:artist.index("(")-1]] += 1
      if "(G)" in artist:
        generated_counts[artist[:artist.index("(")-1]] += 1
    else:
      total_counts[artist] += 1
# if ( in value, add count +1 to index("(") -1
# else, just add count +1 to entire string 
# where the string is the name of the artist (optionally +G +A)

      
# To count total, actual, and generated lyrics...
def count(count_dict):
  s = 0
  for v in count_dict.values():
    s += v
  return(s)

# Evaluation survey
# Name; artists chosen (a, b, c); familiarity with A; with B; with C; generated song artist GUESS; confidence; fluency; actual song artist GUESS; confidence

# read in
# participant object

class participant:
  def __init__(self):
    participant = ""

    # Each participant chooses 3 artists and rates their familiarity
    artist_A = ""; artist_B = ""; artist_C = ""
    fam_A = -1; fam_B = -1; fam_C = -1

    # There is a randomly selected iid source artist for generated
    #   and actual lyrics separately; record truth, guess, and confidence 
    act_artist = ""; gen_artist = ""
    guess_act = ""; guess_gen = ""
    guess_act_conf = -1; guess_gen_conf = -1

    # Participant gauges the fluency of the generated lyrics
    fluency = -1

# To generate plot, need:
# tuple of names
# np array of values (try with just list?)
