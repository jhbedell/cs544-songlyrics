# Author: Ayush Jaiswal

from __future__ import division
from collections import defaultdict
import sys
sys.path.append('../analysis/')
from reader import Reader, Song
from artistProcessor import FeatureExtractor
from numpy.random import choice
from nltk import word_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords as sw
import random
from titleWeightExtraction import generateTitleInChorusToVerseRatio
import re

def generateTitle(lyrics, titleInChorusToVerseRatio):
    '''Generates title for song based on lyrics.'''
    featureExtractor = FeatureExtractor()
    stanzas, structure, lengths = featureExtractor.extractSongStructure(lyrics)
    lines = []
    stanzaIds = []
    for i in range(len(stanzas)):
        stanza = stanzas[i]
        for line in stanza.split('\n'):
            if line != '':
                lines.append(line.split())
                stanzaIds.append(i)
    taggedLines = [pos_tag(line) for line in lines]
    lineLengths = [len(line) for line in lines]  # in terms of word count
    avgLineLength = int(sum(lineLengths) / len(lineLengths))
    nGramFrequencies = {i : defaultdict(int) for i in range(1, (avgLineLength + 1))}
    nGramCumulativeFrequencies = {i : 0 for i in range(1, (avgLineLength + 1))}
    nGramLineMap = {}  # maps nGram to line-index, and the start and end positions in the line
    stopWords = set(sw.words('english'))  # filter unigrams that are stop words
    # Calculate n-gram frequencies:
    for l in range(len(lines)):
        line = lines[l]
        for i in range(len(line)):
            for j in range(1, avgLineLength):
                if j == 1:
                    if word_tokenize(line[i])[0].lower() in stopWords:
                        continue
                    elif i != 0 and line[i][0].isupper():
                        # Increase unigram count of proper nouns naively:
                        word = line[i].lower()
                        nGramFrequencies[1][word] = nGramFrequencies[1][word] + 1
                if (i + j) > len(line):
                    break
                nGramKey = '-'.join([word.lower() for word in line[i:(i+j)]])
                weightedCount = 1
                if stanzaIds[l] == 'C':
                    weightedCount = weightedCount * titleInChorusToVerseRatio
                nGramFrequencies[j][nGramKey] = nGramFrequencies[j][nGramKey] + weightedCount
                # Store the first occurrence of the n-gram:
                try:
                    lineIdx = nGramLineMap[nGramKey]
                except:
                    nGramLineMap[nGramKey] = (l, i, i + j - 1)
    # Find n-grams with the highest relative frequencies:
    bestNGrams = {i : None for i in range(1, (avgLineLength + 1))}
    for i in range(1, (avgLineLength + 1)):
        nGramCumulativeFrequencies[i] = sum([nGramFrequencies[i][key] for key in nGramFrequencies[i].keys()])
        for key in nGramFrequencies[i].keys():
            nGramFrequencies[i][key] = nGramFrequencies[i][key] / nGramCumulativeFrequencies[i]
            if bestNGrams[i] is None or nGramFrequencies[i][key] >= bestNGrams[i][0][1]:
                if bestNGrams[i] is None or nGramFrequencies[i][key] > bestNGrams[i][0][1]:
                    bestNGrams[i] = [(key, nGramFrequencies[i][key])]
                else:
                    bestNGrams[i].append((key, nGramFrequencies[i][key]))
    bestNGramKeys = [key for key in bestNGrams.keys() if bestNGrams[key] is not None]
    # weights that influence the length of the chosen n-gram for the title:
    weights = [bestNGrams[nGramLength][0][1] for nGramLength in bestNGramKeys]
    total = sum(weights)
    weights = [weight / total for weight in weights]
    sampledNGramListFiltered = []
    counter = 0
    # choosing length of title and filtering bad titles:
    # allowed titles end with a noun, a verb, or a pronoun
    while len(sampledNGramListFiltered) == 0:
        length = choice(bestNGramKeys, p=weights)
        sampledNGramList = bestNGrams[length]
        sampledNGramListFiltered = []
        for nGram, weight in sampledNGramList:
            lineIdx, start, end = nGramLineMap[nGram]
            lastTag = taggedLines[lineIdx][end][1]
            if lastTag.startswith('N') or lastTag.startswith('V') or (lastTag.startswith('PR') and lastTag[-1] != '$'):
                sampledNGramListFiltered.append((nGram, weight))
        counter = counter + 1
        if counter == 100:
            break
    if len(sampledNGramListFiltered) == 0:  # if none of the n-grams pass through the filtering
        sampledNGramListFiltered = sampledNGramList
    # sample an n-gram from the allowed n-grams of the sampled length
    sampledNGram = sampledNGramListFiltered[random.randint(0, len(sampledNGramListFiltered) - 1)][0]
    # go back to the line from where this n-gram is picked and slide to the end with the same 'n'
    sampledNGramLength = sampledNGram.count('-') + 1
    sampledNGramLineIdx, start, end = nGramLineMap[sampledNGram]
    bestNGram = (sampledNGram, start)
    for nGram, weight in sampledNGramListFiltered:
        line, start, end = nGramLineMap[nGram]
        if line == sampledNGramLineIdx and bestNGram[1] < start:
            bestNGram = (nGram, start)
    # Capitalize words in the n-gram and return as title
    title = ' '.join([word.capitalize() for word in bestNGram[0].split('-')])
    # Strip punctuation from beginning and end of the title
    title = re.sub('[^a-zA-Z]+$', '', re.sub('^[^a-zA-Z]+', '', title))
    return title

if __name__ == '__main__':
    artistDirectory = '../../data/jamesbedell/'
    r = Reader(artistDirectory)
    songs = r.read()
    titleInChorusToVerseRatio = generateTitleInChorusToVerseRatio(songs)
    for song in songs:
        print 'Original:', song.name
        print 'Generated:', generateTitle(song.lyrics, titleInChorusToVerseRatio)
        print