import sys
import re
import heapq
import math
sys.path.append('../analysis/')
from artist import Artist
from artistProcessor import FeatureExtractor
from reader import Reader, Song
from languagemodel import LanguageModel
from stanzamodel import StanzaModel
from featureKeys import *
from collections import Counter, defaultdict
from paths import pronunciationDictionaryPath
from generatorparameters import *
import titleWeightExtraction, titleGenerator
import random

def flatten(l):
    def flattensub(l):
        for l1 in l:
            for l2 in l1:
                yield l2
    return list(flattensub(l))

def mean(l):
    return sum(l) / float(len(l))

# def low(l):
#     return re.sub(r'[^a-z\' ]', '', l.lower())

class SongGenerator:
    def __init__(self, artistName):
        self.artistName = artistName
        if DEBUG_META: print "Constructing Artist object..."
        self.artist = Artist('../../data/features/'+artistName+'.pkl')
        self.artistDirectory = '../../data/'+artistName+'/'
        
        if DEBUG_META: print "Reading in artist songs..."
        self.artistSongs = Reader(self.artistDirectory).read()
        self.artistTitleRatio = titleWeightExtraction.generateTitleInChorusToVerseRatio(self.artistSongs)
        self.artistLyrics = [re.sub(' +', ' ', song.lyrics).replace('\t', ' ').strip() for song in self.artistSongs]
        self.allStanzas = flatten([song.strip().split('\n\n') for song in self.artistLyrics])
        
        if DEBUG_META: print "Building language model..."
        self.lm = LanguageModel(self.artistLyrics)
        self.lmlinebreaks = LanguageModel([re.sub('\s+', ' ', stanza).strip() for stanza in self.artistLyrics])
        self.vocabulary = self.lm.uni.keys()
        
        if DEBUG_META: print "Building stanza tag model..."
        self.sm = StanzaModel(self.artist)

        self.fe = FeatureExtractor()

        # Models for faster accessing during stanza generation
        # if DEBUG_META: print "Building stanza first line model..."
        # numStanzas = len(self.allStanzas)
        # stanzastartlines = [stanza.split('\n')[0] for stanza in self.allStanzas]
        # self.stanzaFirstLineIncidence = defaultdict(int)
        # for line in stanzastartlines:
        #     for word in line.split(' '):
        #         if word not in self.stanzaFirstLineIncidence:
        #             self.stanzaFirstLineIncidence[word] = 1.0 / numStanzas
        #         else:
        #             self.stanzaFirstLineIncidence[word] += 1.0 / numStanzas

        # if DEBUG_META: print "Building stanza last line model..."
        # stanzaendlines = [stanza.split('\n')[0] for stanza in self.allStanzas]
        # self.stanzaLastLineIncidence = defaultdict(int)
        # for line in stanzaendlines:
        #     for word in line.split(' '):
        #         if word not in self.stanzaLastLineIncidence:
        #             self.stanzaLastLineIncidence[word] = 1.0 / numStanzas
        #         else:
        #             self.stanzaLastLineIncidence[word] += 1.0 / numStanzas

        # if DEBUG_META: print "Building bag-of-words following model [REPLACE THIS WITH BETTER SEMANTICS LATER?]..."
        # allLyrics = '\n\n'.join(self.artistLyrics).split('\n')
        # self.wordfollows = defaultdict(lambda: defaultdict(int))
        # self.numLinesWithWord = {}
        # for i in range(1, len(allLyrics)):
        #     lineA = allLyrics[i-1].strip()
        #     lineB = allLyrics[i].strip()
        #     for wordA in set(lineA.split(' ')):
        #         # Increment the number of lines that this word was eligible to be followed
        #         if wordA not in self.wordfollows:
        #             self.wordfollows[wordA] = defaultdict(int)
        #             self.numLinesWithWord[wordA] = 1.0
        #         else:
        #             self.numLinesWithWord[wordA] += 1.0
        #     if len(lineA) == 0 or len(lineB) == 0:
        #         continue
        #     for wordA in set(lineA.split(' ')):
        #         for wordB in set(lineB.split(' ')):
        #             # Increment the number of times this specific pair followed each other
        #             if wordB not in self.wordfollows[wordA]:
        #                 self.wordfollows[wordA][wordB] = 1.0
        #             else:
        #                 self.wordfollows[wordA][wordB] += 1.0
        
        if DEBUG_META: print "Reading pronunciation dictionary..."
        self.__pronunciationDictionary = self.__readPronunciationDictionary()

        if DEBUG_META: print "Done with constructor."

    def __readPronunciationDictionary(self):
        '''Read pronunciation dictionary from file and return as a dictionary.'''
        pronunciationDictionary = {}
        with open(pronunciationDictionaryPath) as f:
            x = f.read()
            lines = [line for line in x.split('\n') if (line != '' and line[0] != ';')]
            for line in lines:
                word, pronunciation = line.split('  ')
                pronunciationDictionary[word] = pronunciation.split()
        return pronunciationDictionary

    def extractRhymeKeyFromWord_Old(self, word):
        word = re.sub('[^A-Z]', '', word.upper())
        if word not in self.__pronunciationDictionary: return word
        pronunciation = self.__pronunciationDictionary[word]
        idx = len(pronunciation) - 1
        while idx >= 0 and not pronunciation[idx][-1].isdigit():
            idx = idx - 1
        return '-'.join(pronunciation[idx:])

    def generate(self, N=None):
        lyrics = self.__generateTheLyrics(N)
        title = titleGenerator.generateTitle(lyrics, self.artistTitleRatio)
        url = THA_MUTHA_FLIPPIN_URL_YO
        return Song(title, url, lyrics)

    def __generateTheLyrics(self, N=None):
        if N == None: N = NUM_SONGS_TO_GENERATE_AND_RERANK

        candidates = []

        bestsong = None
        bestscore = REALLY_BIG_NUMBER
        for i in range(N):
            song = self.__generateOne()
            score = self.artist.getRerankingScore(song)
            if DEBUG_RERANKING:
                print '-'*100
                print "%2d) THIS SONG SCORE:" % i, score
                print song
                print
            if score < bestscore:
                bestscore = score
                bestsong = song
                if DEBUG_RERANKING_LITE:
                    print "%2d) THIS SONG SCORE:" % i, score, "(new best)"
            else:
                if DEBUG_RERANKING_LITE:
                    print "%2d) THIS SONG SCORE:" % i, score
            candidates.append((song, score))

        qq = 0
        if DEBUG_PRINT_ALL_CANDIDATES:
            for song in sorted(candidates, key=lambda k: k[1]):
                print '='*100
                print '%2d) THIS SONG SCORE:' % qq, song[1]
                print song[0]
                qq += 1

        return re.sub('[()]', '', bestsong)

    def __generateOne(self):
        (stanzaTypes, weight) = self.sm.generateStructure()

        # Sample a length for V, Cx, and R
        lengths = flatten(self.artist.getLengthsList())
        tags = flatten(self.artist.getStructuresList())
        rhymes = self.artist.getRhymeSchemeList()
        rhymes = [[['0']] + rhyme + [['0']] for rhyme in rhymes]
        rhymes = flatten(rhymes)
        lengthsV = []
        lengthsC = []
        lengthsR = []
        rhymesV = []
        rhymesC = []

        for i in range(len(tags)):
            if tags[i] == 'V':
                lengthsV.append(lengths[i])
                rhymesV.append(rhymes[i])
            elif tags[i][0] == 'C':
                lengthsC.append(lengths[i])
                rhymesC.append(rhymes[i])
            elif tags[i] == 'R':
                lengthsR.append(lengths[i])

        lgz = lambda k: len(k) > 0
        rhymesV = filter(lgz, rhymesV)
        rhymesC = filter(lgz, rhymesC)
        rhyV = random.choice(rhymesV)
        rhyC1 = random.choice(rhymesC)
        rhyC2 = random.choice(rhymesC)
        rhyC3 = random.choice(rhymesC)
        rhyC4 = random.choice(rhymesC)
        rhyC5 = random.choice(rhymesC)
        rhyC6 = random.choice(rhymesC)
        lenV = len(rhyV)
        lenC1 = len(rhyC1)
        lenC2 = len(rhyC2)
        lenC3 = len(rhyC3)
        lenC4 = len(rhyC4)
        lenC5 = len(rhyC5)
        lenC6 = len(rhyC6)
        try:
            lenR = random.choice(lengthsR)
        except IndexError:
            lenR = DEFAULT_LEN_R

        generatedStanzas = []

        # Now generate the stanzas one at a time
        numStanzas = len(stanzaTypes)
        if DEBUG_OUTPUTTAGS:
            print stanzaTypes
            print 'lenV=%d, lenC1=%d, lenC2=%d, lenC3=%d, lenC4=%d' % (lenV, lenC1, lenC2, lenC3, lenC4)
        for i in range(numStanzas):
            stanzaType = stanzaTypes[i]
            if DEBUG_OUTPUTHEADERS: print '====='+stanzaType+'====='
            
            # Now generate a stanza of this type!
            if stanzaType == '#':
                stanza = ''
            elif stanzaType == 'V':
                stanza = self.__generateStanza(lenV, rhyV, 'V')
            elif stanzaType.startswith('C'):
                stanza = None
                for j in range(i):
                    if stanzaTypes[j] == stanzaType:
                        # print 'Stanza %d was %s, and this is %s: THE SAME!!!1!!' % (j, stanzaTypes[j], stanzaType)
                        stanza = generatedStanzas[j]
                        break
                    else:
                        # print 'Stanza %d was %s, but this is %s' % (j, stanzaTypes[j], stanzaType)
                        pass
                if stanza == None:
                    if stanzaType == 'C1':
                        stanza = self.__generateStanza(lenC1, rhyC1, 'C1')
                    elif stanzaType == 'C2':
                        stanza = self.__generateStanza(lenC2, rhyC2, 'C2')
                    elif stanzaType == 'C3':
                        stanza = self.__generateStanza(lenC3, rhyC3, 'C3')
                    elif stanzaType == 'C4':
                        stanza = self.__generateStanza(lenC4, rhyC4, 'C4')
                    elif stanzaType == 'C5':
                        stanza = self.__generateStanza(lenC4, rhyC4, 'C5')
                    elif stanzaType == 'C6':
                        stanza = self.__generateStanza(lenC4, rhyC4, 'C6')
                    else:
                        raise ValueError("What kind of chorus is a %s?" % stanzaType)
                else:
                    # print "Therefore, no generation is happening!"
                    pass
            elif stanzaType == 'L':
                stanza = self.__generateStanza(1, ['1'], 'L')
            elif stanzaType == 'R':
                stanza = self.__generateStanza(lenR, None, 'R')
            elif stanzaType == '$':
                stanza = ''
            else:
                raise ValueError('Illegal stanza type: %s' % stanzaType)

            stanza = str(stanza)
            generatedStanzas.append(stanza)
            
            # if stanzaType in ['L', 'R']:
            #     print stanza
            # print stanza
        # End of for loop

        return '\n\n'.join(generatedStanzas).strip()

    def __generateStanza(self, length, rhyme, tag):
        if tag == 'L':
            return ' '.join(self.lm.generateLine()[0])+'\n'
        elif tag == 'R':
            line = ' '.join(self.lm.generateLine()[0])
            return ((line+'\n')*length)
        if DEBUG_OUTPUTHEADERS: print 'Stanza of type %s with length %d and rhyme %s' % (tag, length, rhyme)

        candidates = [None] * NUM_STANZA_CANDIDATES
        for q in range(NUM_STANZA_CANDIDATES):
            while True:
                bad = False
                weight = 0.0
                genLines = [None] * length
                assert len(rhyme) == length
                genLines[0] = self.__generateFirstLine(tag)
                # print self.lmlinebreaks.tri
                # print genLines[0]
                i = 1
                for i in range(1, length):
                    thisrhyme = None
                    for j in range(i):
                        if rhyme[j] == rhyme[i]:
                            thisrhyme = self.fe.extractRhymeKeyFromWord(genLines[j][0][-1])
                            break
                    if len(genLines[i-1][0]) < MIN_FOLLOW_LINE_LEN:
                        bad = True
                        break
                    genLines[i] = self.__generateFollowingLine(genLines[i-1], tag, thisrhyme)
                    # print genLines[i]
                    if genLines[i][0] == None:
                        bad = True # Want to start over, nothing rhymed with first line
                        break
                    weight += math.log(genLines[i][1])
                if bad: continue
                break
            candidates[q] = (genLines, weight)

        bestcandidate = None
        bestweight = -REALLY_BIG_NUMBER
        for candidate, candidateweight in candidates:
            if candidateweight > bestweight:
                bestcandidate = candidate
                bestweight = candidateweight
        # print bestcandidate, bestweight
        bestcandidate = '\n'.join([' '.join(line[0]) for line in bestcandidate])
        return bestcandidate

    def __generateFirstLine(self, tag):
        return self.lm.generateLine()[0], 1.0

        stanzastartlines = [stanza.split('\n')[0].strip() for stanza in self.allStanzas]
        stanzastartlinelens = [len(l.split(' ')) for l in stanzastartlines if len(l) > 0]
        stanzastartlinelens = [l for l in stanzastartlinelens if l >= MIN_START_LINE_LEN and l < MAX_START_LINE_LEN]
        target = random.choice(stanzastartlinelens)

        n = 0
        while True:
            n += 1
            line, weight = self.lm.generateLine()
            if len(line) == target:
                # print "Took %d tries to generate a line" % n
                return (line, 1.0)

    def __generateFollowingLine(self, lastline, tag, rhyme=None):
        # print '='*70
        # print "Generating line to follow:\n  ", ' '.join(lastline[0])
        # print "  Rhymes with", rhyme
        candidates = [None] * NUM_FOLLOWING_LINES
        for i in range(NUM_FOLLOWING_LINES):
            bad = True
            for j in range(GIVE_UP_AFTER):
                newword0 = newword1 = '<EOL>'
                while newword0 == '<EOL>':
                    # WARNING THIS MAY INFINITELOOP IF self.lmlinebreaks.uni_wt IS SET TO ZERO
                    assert self.lmlinebreaks.uni_wt > 0
                    newword0, weight0 = self.lmlinebreaks.generateWord([lastline[0][-2], lastline[0][-1]])
                while newword1 == '<EOL>':
                    newword1, weight1 = self.lmlinebreaks.generateWord([lastline[0][-1], newword0])
                newline, newweight = self.lm.generateLine(lastBigram=[newword0, newword1])
                # print " #%d:" % j, ' '.join([newword0, newword1] + newline)
                if len(newline) == 0: continue
                # print newline
                if (rhyme == None) or (self.fe.extractRhymeKeyFromWord(newline[-1]) == rhyme):
                    bad = False
                    # if j > 0: print "Got a good one after %d iterations" % j
                    break
            if bad:
                # print "Gave up after %d iterations" % GIVE_UP_AFTER
                # print "Never got something that rhymes :("
                candidates[i] = (None, -1)
                continue
            candidate = [newword0, newword1] + newline
            lenratio = len(lastline[0])/float(len(candidate))
            candidateweight = weight0 * weight1 * (lenratio if lenratio <= 1 else 1.0/lenratio)
            candidates[i] = (candidate, candidateweight)
            # print 'CANDIDATE:', ' '.join(candidate), candidateweight, weight0, weight1, lenratio

        # print '-'*80
        # print "ALL CANDIDATES:"
        # for c in candidates:
        #     print c
        bestcandidate = None
        bestweight = 0
        for candidate, candidateweight in candidates:
            if candidateweight > bestweight:
                bestcandidate = candidate
                bestweight = candidateweight
        # print bestcandidate, bestweight
        return (bestcandidate, bestweight)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        artist = TESTARTIST
    elif len(sys.argv) == 2:
        artist = sys.argv[1]

    print >> sys.stderr, "Generating a song in the style of %s...\n" % artist

    sg = SongGenerator(artist)
    song = sg.generate()

    s = '"%s" by %s:' % (song.name, artist)
    print s
    print '-'*len(s)
    print song.lyrics