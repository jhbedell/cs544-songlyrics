NUM_SONGS_PER_ARTIST=40

for i in $(seq 7 $NUM_SONGS_PER_ARTIST)
do
    for artist in $(cat ../../data/artists.txt)
    do
        echo Generating song $i for $artist...
        python songgenerator.py $artist > generatedsongs/$artist$i.txt
    done
done