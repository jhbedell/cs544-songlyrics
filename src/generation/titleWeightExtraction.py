# Author: Ayush Jaiswal

from __future__ import division
import sys
sys.path.append('../analysis/')
from reader import Reader, Song
from artistProcessor import FeatureExtractor

def generateTitleInChorusToVerseRatio(songs):
    '''Generates ratio of frequency of title in chorus to frequency of title in verse for the artist.'''
    featureExtractor = FeatureExtractor()
    titleInChorusFrequency = 1
    titleInVerseFrequency = 1
    for song in songs:
        stanzas, structure, lengths = featureExtractor.extractSongStructure(song.lyrics)
        structure = structure[1:-1]
        title = song.name.lower()
        for i in range(len(stanzas)):
            stanza = stanzas[i].lower()
            if structure[i] == 'V':
                titleInVerseFrequency = titleInVerseFrequency + stanza.count(title)
            elif structure[i] == 'C':
                titleInChorusFrequency = titleInChorusFrequency + stanza.count(title)
    return (titleInChorusFrequency / titleInVerseFrequency)