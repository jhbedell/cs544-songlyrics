import sys
import pickle
import re
import random
sys.path.append('../analysis/')
from reader import Reader
from generatorparameters import *

class LanguageModel:
    '''This class constructs a language model from the songs of one artist.'''

    def __init__(self, corpus):
        '''Ye olde initializer.  Builds the whole LM here.'''

        self.tri_wt = TRI_WEIGHT
        self.bi_wt = BI_WEIGHT
        self.uni_wt = UNI_WEIGHT

        self.corpus = corpus
        d = {} # trigram dict
        bd = {} # bigram dict
        ud = {} # unigram dict

        for song in corpus:
            # print song.name
            for line in song.strip().split('\n'):
                if len(line.strip()) == 0: continue

                line = '<PBOL> <BOL> ' + line.strip() + ' <EOL>'
                lineparts = [w.strip() for w in re.split(' +', line)]

                for i in range(2, len(lineparts)):
                    if len(lineparts[i-2]) == 0:
                        print line
                    # trigram
                    if lineparts[i-2] not in d:
                        d[lineparts[i-2]] = {}
                    if lineparts[i-1] not in d[lineparts[i-2]]:
                        d[lineparts[i-2]][lineparts[i-1]] = {}
                    if lineparts[i] not in d[lineparts[i-2]][lineparts[i-1]]:
                        d[lineparts[i-2]][lineparts[i-1]][lineparts[i]] = 1
                    else:
                        d[lineparts[i-2]][lineparts[i-1]][lineparts[i]] += 1

                    # bigram
                    if lineparts[i-1] != '<PBOL>':
                        if lineparts[i-1] not in bd:
                            bd[lineparts[i-1]] = {}
                        if lineparts[i] not in bd[lineparts[i-1]]:
                            bd[lineparts[i-1]][lineparts[i]] = 1
                        else:
                            bd[lineparts[i-1]][lineparts[i]] += 1

                    # unigram
                    if lineparts[i] not in ['<PBOL>', '<BOL>', '<EOL>']: # Do I want to generate EOL?
                        if lineparts[i] not in ud:
                            ud[lineparts[i]] = 1
                        else:
                            ud[lineparts[i]] += 1
                    ud['<EOL>'] = 0


        w = {} # trigram weight
        bw = {} # bigram weight
        uw = {} # unigram weight

        # Trigram weights
        for a in d:
            w[a] = {}
            for b in d[a]:
                w[a][b] = {}
                s = float(sum(d[a][b].values()))
                for c in d[a][b]:
                    w[a][b][c] = d[a][b][c] / s

        # Bigram weights
        for a in bd:
            bw[a] = {}
            s = float(sum(bd[a].values()))
            for b in bd[a]:
                bw[a][b] = bd[a][b] / s

        # Unigram weights
        s = float(sum(ud.values()))
        for a in ud:
            uw[a] = ud[a] / s

        self.tri = w
        self.bi = bw
        self.uni = uw

        # Empirical CDFs for sampling
        self.tri_cdf = {}
        for a in self.tri:
            self.tri_cdf[a] = {}
            for b in self.tri[a]:
                self.tri_cdf[a][b] = []
                totalweight = 0.0
                for c, weight in self.tri[a][b].items():
                    totalweight += weight
                    self.tri_cdf[a][b].append((c, totalweight))

        self.bi_cdf = {}
        for a in self.bi:
            self.bi_cdf[a] = []
            totalweight = 0.0
            for c, weight in self.bi[a].items():
                totalweight += weight
                self.bi_cdf[a].append((c, totalweight))

        self.uni_cdf = []
        totalweight = 0.0
        for c, weight in self.uni.items():
            totalweight += weight
            self.uni_cdf.append((c, totalweight))

        # End of constructor
        ##############################################################################

    def write(self, filename):
        '''Save this language model to a pickle file.'''
        with open(filename, 'w') as f:
            pickle.dump((self.tri, self.bi, self.uni), f)

    def printWFSA(self, filename, tri_wt = None, bi_wt = None, uni_wt = None, unigrams=None, backoff=True):
        '''Write this language model as a Carmel-format WFSA.'''
        
        if tri_wt == None: tri_wt = self.tri_wt
        if bi_wt == None: bi_wt = self.bi_wt
        if uni_wt == None: uni_wt = self.uni_wt

        def nq(a):
            '''Printing everything surrounded in quotes, so have to escape actual quotes.'''
            a = re.sub('[()"*]', '', a)
            a = re.sub(r'\!', r'\\X', a)
            return a

        print "Writing trigram WFSA to %s." % filename
        if backoff == False: tri_wt = 1.0

        # Because this is easier than find-replacing everything I copy-pasted
        tw = self.tri
        bw = self.bi
        uw = self.uni

        # Pass in a unigrams parameter to restrict what trigrams actually get printed out!
        if unigrams == None:
            unigrams = uw.keys()
        unigrams = set(unigrams)
        unigrams.update(['<EOL>', '<PBOL>', '<BOL>'])

        # Open output file and write to it!
        with open(filename, 'w') as f:
            f.write('<EOL>\n')
            f.write('(<PBOL> (<PBOL>::<BOL>))\n')

            # trigrams
            print 'Writing trigrams...'
            for a in tw:
                if a not in unigrams: continue
                if a == '<PBOL>':
                    b = '<BOL>'
                    f.write('(%s::%s (:%s:))' % (nq(a), nq(b), nq(b)) + '\n')
                    for c in tw[a][b]:
                        if c not in unigrams: continue
                        if c != '<EOL>':
                            f.write('(%s::%s (%s::%s "%s" "%s" %.20f))' % (nq(a), nq(b), nq(b), nq(c), nq(c), nq(c), tw[a][b][c]*tri_wt) + '\n')
                else:
                    for b in tw[a]:
                        if b not in unigrams: continue
                        f.write('(%s::%s (:%s:))' % (nq(a), nq(b), nq(b)) + '\n')
                        for c in tw[a][b]:
                            if c not in unigrams: continue
                            if c != '<EOL>':
                                f.write('(%s::%s (%s::%s "%s" "%s" %.20f))' % (nq(a), nq(b), nq(b), nq(c), nq(c), nq(c), tw[a][b][c]*tri_wt) + '\n')
                            else:
                                f.write('(%s::%s (%s *e* *e* %.20f))' % (nq(a), nq(b), nq(c), tw[a][b][c]*tri_wt) + '\n')

            if backoff:
                # bigrams
                print 'Writing bigrams...'
                for a in bw:
                    if a not in unigrams: continue
                    if a == '<PBOL>':
                        continue
                    f.write('(:%s: (<UNI>))' % (nq(a)) + '\n')
                    for b in bw[a]:
                        if b not in unigrams: continue
                        if b != '<EOL>':
                            f.write('(:%s: (%s::%s "%s" "%s" %.20f))' % (nq(a), nq(a), nq(b), nq(b), nq(b), bw[a][b]*bi_wt) + '\n')
                        else:
                            f.write('(:%s: (%s *e* *e* %.20f))' % (nq(a), nq(b), bw[a][b]*bi_wt) + '\n')

                # unigrams
                print 'Writing unigrams...'
                for a in uw:
                    if a not in unigrams: continue
                    if a not in ['<PBOL>', '<BOL>', '<EOL>']:
                        f.write('(<UNI> (:%s: "%s" "%s" %.20f))' % (nq(a), nq(a), nq(a), uw[a]*uni_wt) + '\n')

    def generateLine(self, tri_wt = None, bi_wt = None, uni_wt = None, unigrams=None, backoff=True, lastBigram=None):
        '''Generate one line from this LM, starting with <BOL> and sampling at random until <EOL> is reached.'''
        if tri_wt == None: tri_wt = self.tri_wt
        if bi_wt == None: bi_wt = self.bi_wt
        if uni_wt == None: uni_wt = self.uni_wt
        if backoff == False: tri_wt = 1.0

        last = ['<PBOL>', '<BOL>'] if lastBigram == None else lastBigram
        generated = []
        totalweight = 1.0

        # Keep picking words until we get an EOL
        while True:
            (newword, newweight) = self.generateWord(last, tri_wt, bi_wt, uni_wt, unigrams, backoff)

            totalweight *= newweight

            if newword == '<EOL>':
                break

            generated.append(newword)

            last[0] = last[1]
            last[1] = newword

        return (generated, totalweight)

    def generateWord(self, last, tri_wt = None, bi_wt = None, uni_wt = None, unigrams=None, backoff=True):
        # print "Generating the next word given", last
        if tri_wt == None: tri_wt = self.tri_wt
        if bi_wt == None: bi_wt = self.bi_wt
        if uni_wt == None: uni_wt = self.uni_wt
        if backoff == False: tri_wt = 1.0

        pmass = 0.0
        target = random.random()
        origtarget = target

        # Generate trigrams
        if target < tri_wt:
            target = target / tri_wt
            try:
                tridict = self.tri_cdf[last[0]][last[1]]
                for (key, val) in tridict:
                    if val > target:
                        weight = self.tri[last[0]][last[1]][key]*tri_wt + self.bi[last[1]][key]*bi_wt + self.uni[key]*uni_wt
                        return key, weight
            except KeyError:
                # print "GLORIOUS KEY ERROR"
                # print "Right now, target=%f, pmass=%f" % (target, pmass)
                target = target * (bi_wt + uni_wt)
        else:
            target = target - self.tri_wt


        # print "After trigrams, target=%f, pmass=%f" % (target, pmass)

        if target < bi_wt:
            target = target / bi_wt
            bidict = self.bi_cdf[last[1]]
            for (key, val) in bidict:
                if val > target:
                    weight = self.bi[last[1]][key]*bi_wt + self.uni[key]*uni_wt
                    return key, weight
        else:
            target = target - self.bi_wt

        if target < uni_wt:
            target = target / uni_wt
            unidict = self.uni_cdf
            for (key, val) in unidict:
                if val > target:
                    weight = self.uni[key]*uni_wt
                    return key, weight
        else:
            raise ValueError("Target was %f, and was higher than tri_wt+bi_wt+uni_wt!" % origtarget)

        if target > pmass:
            print "target: ", target
            print "pmass: ", pmass
            raise ValueError('Should have probability mass 1 now!')
        else:
            raise RuntimeError('Seriously, how the fuck did you get here?  target=%f, pmass=%f' % (target, pmass))

    def getWeight(self, word, prevword=None, prevprevword=None):
        weight = 0.0
        try:
            weight += self.tri[prevprevword][prevword][word] * self.tri_wt
        except KeyError:
            pass
        try:
            weight += self.bi[prevword][word] * self.bi_wt
        except KeyError:
            pass
        try:
            weight += self.uni[word] * self.uni_wt
        except KeyError:
            pass
        return weight

# End of LanguageModel class
######################################################################################

def load(filename):
    '''Load a language model from a file.  Returns a LanguageModel object.'''
    with open(filename) as f:
        (tri, bi, uni) = pickle.load(f)
        lm = LanguageModel(None)
        lm.tri = tri
        lm.bi = bi
        lm.uni = uni
    return lm

if __name__ == '__main__':
    artistDirectory = '../../data/%s/' % TESTARTIST

    songs = Reader(artistDirectory).read()
    lm = LanguageModel([song.lyrics for song in songs])
    # lm.printWFSA('../../data/languagemodels/cash.wfsa')

    for i in range(50):
        line, weight = lm.generateLine()
        print ' '.join(line), weight