import sys
sys.path.append('../analysis/')
from artist import Artist
from languagemodel import LanguageModel
from featureKeys import *
from collections import Counter
import random

class StanzaModel(LanguageModel):

    def __init__(self, artist):
        self.artist = artist
        artist_structures = artist.getStructuresList()
        LanguageModel.__init__(self, [' '.join(i) for i in artist_structures])

    def generateStructure(self, length=None):
        if length == None:
            (line, wt) = self.generateLine(backoff=False)
        else:
            line = []
            while len(line) != length:
                (line, wt) = self.generateLine(backoff=False)

        return (line, wt)

if __name__ == '__main__':
    artistDirectory = '../../data/features/cash.pkl'

    artist = Artist(artistDirectory)
    sm = StanzaModel(artist)
    
    # for i in range(50):
    #     print sm.generateStructure()

    sm.generateSong()
    # lengths = flatten(artist.getLengthsList())
    # tags = flatten(artist.getStructuresList())
    # rhymes = artist.getRhymeSchemeList()
    # rhymes = [[['0']] + rhyme + [['0']] for rhyme in rhymes]
    # print len(rhymes)
    # print len(lengths)
    # tags = [' '.join(i) for i in flatten(rhymes)]

    # print rhymes[0]
    # print rhymes[33]
    # print lengths[0]

    # for l in sorted(set(lengths)):
    #     print l
    #     print Counter([len(tags[i].split()) for i in range(len(tags)) if lengths[i] == l])
    # print lengths