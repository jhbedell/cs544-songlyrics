
# LANGUAGE MODEL
TRI_WEIGHT = 0.9
BI_WEIGHT = 0.099
UNI_WEIGHT = 0.001

# GENERATOR
TESTARTIST = 'cash'
NUM_SONGS_TO_GENERATE_AND_RERANK = 10
NUM_START_LINES                  = None
NUM_CANDIDATE_LINES              = None    # Number of lines used to form each stanza
NUM_FOLLOWING_LINES              = 10     # Number of lines each line is followed by in the lattice
NUM_STANZA_CANDIDATES            = 3     # Number of stanzas generated that fit the rhyme scheme
REPEATED_LINE_PENALTY            = None    # Multiplicative penalty on line following score (lower score is better)
LENGTH_MULTIPLIER_BASE           = None    # raised to the power of abs(lenA - lenB), higher score is better
GIVE_UP_AFTER                    = 100
MIN_START_LINE_LEN               = 3
MAX_START_LINE_LEN               = 20
MIN_FOLLOW_LINE_LEN              = 2

# In case of no R in input data
DEFAULT_LEN_R = 3

# Big?
REALLY_BIG_NUMBER = 2**30 - 1 + 2**30 # largest 32-bit signed integer

# These thresholds are necessary to prune down the search space, or else shit gets really out of whack
# START_SCORE_THRESHOLD = 0.99
# START_SCORE_EXPONENT = 5.0

# FOLLOW_SCORE_THRESHOLD = 1.99
# FOLLOW_SCORE_EXPONENT = 5.0
# FOLLOW_LM1_THRESHOLD = 1.99
# FOLLOW_LM1_EXPONENT = 5.0
# FOLLOW_LM2_THRESHOLD = 1.99
# FOLLOW_LM2_EXPONENT = 5.0

# END_SCORE_THRESHOLD = 0.99
# END_SCORE_EXPONENT = 5.0

# debug songgenerator
DEBUG_META = False
DEBUG_OUTPUTTAGS = False
DEBUG_OUTPUTHEADERS = False
DEBUG_CANDIDATES = False
DEBUG_FOLLOWSCORE = False
DEBUG_STARTSCORE = False
DEBUG_ENDSCORE = False
DEBUG_POP = False
DEBUG_RHYMEFIT = False
DEBUG_POSSIBLES = False
DEBUG_RERANKING = False
DEBUG_RERANKING_LITE = False
DEBUG_PRINT_ALL_CANDIDATES = False



















    

# don't mind me, I'm just a parameter
THA_MUTHA_FLIPPIN_URL_YO = 'http://www.facebook.com/VerminSupreme'
