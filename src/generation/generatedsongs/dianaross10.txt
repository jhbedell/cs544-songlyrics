"Ain't Nothing Maybe, Baby" by dianaross:
-----------------------------------------
In our dreams
You are everything
And everything is groovy

Does she know just where you're going to
Do you know you'd have to fee the pain
Keep on, keep on, keep on, keep on dancing

The time to live without care
I'm broke, it's so
And everything must live

I, I will always part of love eternally
There love will be right here and go
Listen, passing through the sunsets and the years

Sky above and earth below
You can't do without, oh no
His sweet tender kiss has got to let it flow
Ain't nothing maybe, baby
Forget this hunger inside
Should I let go

I know that I'm sure about
That certain kind of a man before
Never held him in a storm

Trying to hide it
Rekindled flames are not recommended
But it never really mattered

Sky above and earth below
You can't do without, oh no
His sweet tender kiss has got to let it flow
Ain't nothing maybe, baby
Forget this hunger inside
Should I let go

Sky above and earth below
You can't do without, oh no
His sweet tender kiss has got to let it flow
Ain't nothing maybe, baby
Forget this hunger inside
Should I let go
