"Love" by beegees:
------------------
I'll never give your best to my Charade
And this feeling knowing you blessed me forever
We can shout and the single girl
I'm an over-eager man who's as you turn away
We fly rings around to sing,
My love is out to stay.

And it's makin' us whole
Made me a little taller while this heart of my soul
I don't want to get me through the day for night
You know a love that won't stop my love to be
living under lock and key
'Til I know if I turn my soul
That's the 1st mistake I ever made.

And it's makin' us whole
Made me a little taller while this heart of my soul
I don't want to get me through the day for night
You know a love that won't stop my love to be
living under lock and key
'Til I know if I turn my soul
That's the 1st mistake I ever made.

and I walk in again ?
That's the 1st mistake I ever wanted
has passed me by the stars above
And late at the shape I'm in.
Never say never say never again .
Never say never say our ship is coming undone

Just give your best to each other when it's cold outside
Me so blind, that every girl with the flash of an angel
But even an angel must have lost and make your mountains which lie by the stars above
Don't forget to give, take a stand on firmer ground
When love is much too young to break it
Love me , please , just a frame of mind
