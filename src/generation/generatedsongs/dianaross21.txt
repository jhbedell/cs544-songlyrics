"They Say" by dianaross:
------------------------
But you came
I feel I'm hypnotized
I wanna ask you
Do you really love you

They say that
Somehow it just loneliness
We ought to get along
Tried hard to be strong

I can't get mine
They say, so they say
If it's there I take
You're every hour, every day

'Cause I'm in the dark side of the dell
Starts to ring Ding ding ding
When the valley's deep I'll be there
Everyone needs someone to share

I can't get mine
They say, so they say
If it's there I take
You're every hour, every day

All night, baby all night
Keep it there baby
All night, baby all night
Keep it there baby

I've been waiting
too long unsung
Or a soul forever young
Stay forever young

So long after you?e gone
Oooh, precious memories linger on
So long after you?e gone
Oooh, precious memories that all the loving people gone

Why must we wait so long since I've
Been with a fat one, trying to forget
The way I love you more today than yesterday
But not as much as yesterday

I can't get mine
They say, so they say
If it's there I take
You're every hour, every day
