"Of Love" by tonybennett:
-------------------------
In a highfalutin style
So long, so long
for a star
Carry moonbeams home in a web of love song

When a Broadway baby says good night
It's early in the beginning of love
Yes, I'm trapped in a web of love
Yes, I'm trapped in a web of love

To cuddle and fold near to you
Body and black too
Watch your spirits climb.
Shall we dance with you

Or will I pass her by
No hopeful rainbow in the sky
Then, whenever you're through
Painted ponies wait for you and I

Murmur to the things you love
An' we can face the unknown
I'll build a world of mine
I'm all alone
