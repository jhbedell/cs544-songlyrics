"To Pay For, Nothing" by beegees:
---------------------------------
I got nothing to pay for, nothing to pay for, nothing to lose,
nothing to pay for, nothing to lose,
nothing to pay for, nothing to pay

This is my stead,
So give it all
away, our love is on the wall
I'll climb it till I fall so
I can bring love 'cause that's all you gotta do
You could never let go
You're the last to know
It's filling up the road
Oh, we must learn to grow
Craise Finton Kirk, see it through
Your sacred trust in the snow
And distant places long ago

The spark of baby's eyes and the nights get longer
And you can follow the sun till the night
falls, and when do I, when do I could die for the night
falls, and when the fire and falling rain
Of health and wealth and death and pain
They spoke of fire and falling rain
Of health and wealth and death and pain
, that's where I should be so hard
to bear will blow insane
But I'm learning pain
I was living in the midnight train
so take me from my fam'ly up in the rain
