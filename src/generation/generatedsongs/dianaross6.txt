"Overtime, Dedicated To Know" by dianaross:
-------------------------------------------
No it won't, no it won't be ringin'
But who is right, who was so unexpected
I could take what they told you was free

I felt a sudden chill
'Cause I love you so
I can prove it
Workin' overtime, dedicated to know
Got to let me go
What a feeling this is to go

I felt a sudden chill
'Cause I love you so
I can prove it
Workin' overtime, dedicated to know
Got to let me go
What a feeling this is to go

All, all over his body
Make him strong enough
From his head down

I felt a sudden chill
'Cause I love you so
I can prove it
Workin' overtime, dedicated to know
Got to let me go
What a feeling this is to go

It will reach defeat
Tell me when, where
How did you cold, not sweet
'Cause if you will be there
Everyone needs someone to share
Is all I'm still waiting here

I felt a sudden chill
'Cause I love you so
I can prove it
Workin' overtime, dedicated to know
Got to let me go
What a feeling this is to go

I have been denied
Had you been around
Had you an ultimatum

I guess I had their tongues out
To play some dates with Billy Bates
He gave his notice to Ragtime Otis

If leaves have to make a wish
And this moment would be best yet
If you dream it strong enough

I felt a sudden chill
'Cause I love you so
I can prove it
Workin' overtime, dedicated to know
Got to let me go
What a feeling this is to go
