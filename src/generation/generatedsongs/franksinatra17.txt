"Know That He'll Be" by franksinatra:
-------------------------------------
For the one she passes goes - I am so happy to know that he'll be tall and thin
Five feet seven - of bones and skin
But when we walk with the Continental
But this feeling isn't purely mental

Someone to whisper "Darling you're my baby and one who once knew it, she's not the type to hang around
Down around the world around
When you see before you.
Give us your tree be filled with you.
