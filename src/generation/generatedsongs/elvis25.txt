"And Turn You 'bout Kingdom Come" by elvis:
-------------------------------------------
Bright shining as the way they seem
Please don't ever let me dream
If I'm a dreamer, let me dream
If I'm a little bit of green

Me hanging 'round your hive
Ev'ry day at five
And I'm never gonna leave once I arrive
'Cause I'm so hurt to think at all

She'll wring you out and turn you 'bout kingdom come
You know I ask is please, please love me too
I beg you wait a little whipped pup because of you, my boy
You are the great hidden snares often take the chains away that keep me loving you

If we lost true love you dear
Until the day without you near
If you walk away from my dear
So I best be on my bended knees

She'll wring you out and turn you 'bout kingdom come
You know I ask is please, please love me too
I beg you wait a little whipped pup because of you, my boy
You are the great hidden snares often take the chains away that keep me loving you

Will I get so lonely,
I get so lonely,
I get so lonely,
I get home Cindy, Cindy

And make believe you love me true
Make me know and I'll never part from you
Yeah I'll hold you in a sky more blue
If she came I feel so good rockin' tonight.
